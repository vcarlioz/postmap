/*Copyright (c) 2016, Andrew Walz.
 
 Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

import UIKit
import SnapSliderFilters
import AVFoundation
import AVKit
import MobileCoreServices
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import MapKit

class VideoViewController: UIViewController, CLLocationManagerDelegate {
    
    fileprivate let screenView:UIView = UIView(frame: CGRect(origin: CGPoint.zero, size: SNUtils.screenSize))
    fileprivate let slider:SNSlider = SNSlider(frame: CGRect(origin: CGPoint.zero, size: SNUtils.screenSize))
    fileprivate let textField = SNTextField(y: SNUtils.screenSize.height/2, width: SNUtils.screenSize.width, heightOfScreen: SNUtils.screenSize.height)
    fileprivate let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer()

    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private var videoURL: URL
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    
    init(videoURL: URL) {
        self.videoURL = videoURL
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var buttonSave = SNButton(frame: CGRect(x: 20, y: SNUtils.screenSize.height - 35, width: 33, height: 30), withImageNamed: "saveButton")

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.gray
        player = AVPlayer(url: videoURL)
        playerController = AVPlayerViewController()
        
        guard player != nil && playerController != nil else {
            return
        }
        playerController!.showsPlaybackControls = false
        
        playerController!.player = player!
        self.addChildViewController(playerController!)
        self.view.addSubview(playerController!.view)
        playerController!.view.frame = view.frame
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)

        
        tapGesture.addTarget(self, action: #selector(handleTap))
        
        //setupSlider()
        
        
        
        let cancelButton = UIButton(frame: CGRect(x: 20.0, y: 10.0, width: 30.0, height: 30.0))
        cancelButton.setImage(#imageLiteral(resourceName: "cancel"), for: UIControlState())
        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        setupButtonSave()
        
        
        //snapfilters set up
        //setupSlider()
        setupTextField()
        self.view.addSubview(screenView)
        self.view.bringSubview(toFront: self.screenView)
        view.addSubview(cancelButton)
        view.addSubview(buttonSave)

        
        // original postmap camera view viewdidload
        
        self.checkLocationAuthorizationStatus()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy=kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }

    
    fileprivate func setupTextField() {
        self.screenView.addSubview(textField)
        
        self.tapGesture.delegate = self
        //self.slider.addGestureRecognizer(tapGesture)
        self.screenView.addGestureRecognizer(tapGesture)

        
        NotificationCenter.default.addObserver(self.textField, selector: #selector(SNTextField.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self.textField, selector: #selector(SNTextField.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self.textField, selector: #selector(SNTextField.keyboardTypeChanged(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    fileprivate func setupButtonSave() {
        self.buttonSave.setAction {
            
            [weak weakSelf = self] in
                print("video detected")
            self.videoWithFilter(videoURL: self.videoURL)

        }
        
        self.view.addSubview(self.buttonSave)
    }
    
    func uploadVideo (videoURLToUpload : URL){
        self.postLocation()
        print("VIDEO URL TO UPLOAD")
        print(videoURLToUpload)
        
        let postUIID = NSUUID.init()
        let postUIIDString = postUIID.uuidString
        let postTimeStamp = Timestamp
        
        
        
        let imagesRef = self.storage.reference()
        let postRef = imagesRef.child("images/\(postUIIDString).mov")
        
        
        let uploadTask = postRef.putFile(videoURLToUpload, metadata: nil){ metadata, error in
            if let error = error {
                print(error)
            } else {
                let downloadURL = metadata!.downloadURL()
                print("video upload")
                self.ref.child("post").childByAutoId().setValue(["postUIID": postUIIDString,"latitude":self.postLatitude,"longitude":self.postLongitude,"media type":"movie","posted by":MyVariables.currentUserName,"time stamp":postTimeStamp])
                self.player?.pause()
                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                
            }
        }
    }
    
    func videoWithFilter(videoURL : URL){

        let composition = AVMutableComposition()
        let vidAsset = AVURLAsset(url: videoURL)
        if vidAsset.videoOrientation().orientation == .portrait{
            print("portrait")
        }
        else{
            print("not portrait")
        }
        // get video track
        let vtrack =  vidAsset.tracks(withMediaType: AVMediaTypeVideo)
        let videoTrack:AVAssetTrack = vtrack[0] 
        let vid_duration = videoTrack.timeRange.duration
        let vid_timerange = CMTimeRangeMake(kCMTimeZero, vidAsset.duration)

        //get audio
        let atrack =  vidAsset.tracks(withMediaType: AVMediaTypeAudio)
        let audioTrack:AVAssetTrack = atrack[0]
        let audio_duration = audioTrack.timeRange.duration
        let audio_timerange = CMTimeRangeMake(kCMTimeZero, vidAsset.duration)
        
        do {
        let compositionvideoTrack:AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaTypeVideo, preferredTrackID: CMPersistentTrackID())
        try compositionvideoTrack.insertTimeRange(vid_timerange, of: videoTrack, at: kCMTimeZero)
        compositionvideoTrack.preferredTransform = videoTrack.preferredTransform
            
            let compositionAudioTrack:AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaTypeAudio, preferredTrackID: CMPersistentTrackID())
            try! compositionAudioTrack.insertTimeRange(audio_timerange, of: audioTrack, at: kCMTimeZero)
            
            compositionvideoTrack.preferredTransform = audioTrack.preferredTransform
        
        } catch{
            print(error)
        }
        
        //create video layers
        let size = videoTrack.naturalSize
        print("size")
        print(size)
        let videolayer = CALayer()
        videolayer.frame = CGRect(x:0, y:0, width: size.height, height: size.width)
        let parentlayer = CALayer()
        parentlayer.frame = CGRect(x:0, y:0, width: size.height, height: size.width)

        parentlayer.addSublayer(videolayer)
        let screenviewScaled = self.screenView
        screenviewScaled.transform = CGAffineTransform(scaleX: size.height/self.screenView.frame.size.width, y: size.width/self.screenView.frame.size.height)
        screenviewScaled.layer.frame = CGRect(x:0, y:size.width/self.screenView.frame.size.height, width: size.height, height: size.width)
        parentlayer.addSublayer(screenviewScaled.layer)

        
        let layercomposition = AVMutableVideoComposition()
        
        layercomposition.frameDuration = CMTimeMake(1, 30)
        layercomposition.renderSize = CGSize(width: size.height, height: size.width)
        
        layercomposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videolayer, in: parentlayer)
        
        // instruction for composition
        let instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, composition.duration)
        let transformInstruction:AVMutableVideoCompositionLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
        let translate = CGAffineTransform(translationX: size.height, y: 0)
        let rotate = translate.rotated(by: CGFloat(Double.pi/2))
        transformInstruction.setTransform(rotate, at: kCMTimeZero)
        //instruction.layerInstructions = [transformInstruction]
        //layercomposition.instructions = [instruction]
        let videotrack = composition.tracks(withMediaType: AVMediaTypeVideo)[0] as AVAssetTrack
        let layerinstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videotrack)

        instruction.layerInstructions = [transformInstruction]
        layercomposition.instructions = [instruction]
        
        // create temp file directory
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0] as NSString
        
        let movieFilePath = docsDir.appendingPathComponent("result_composition.mov")
        let movieDestinationUrl = NSURL(fileURLWithPath: movieFilePath)
        do{
        try FileManager.default.removeItem(at: movieDestinationUrl as URL)
        }catch{
            
        }

        
        // Export video
        let assetExport = AVAssetExportSession(asset: composition, presetName:AVAssetExportPresetHighestQuality)
        assetExport?.videoComposition = layercomposition
        assetExport?.outputFileType = AVFileTypeQuickTimeMovie
        assetExport?.outputURL = movieDestinationUrl as URL
        assetExport?.exportAsynchronously(completionHandler: {
                print("Movie complete")
            self.uploadVideo(videoURLToUpload: movieDestinationUrl as URL)
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        player?.play()
    }
    
    func cancel() {
        self.player?.pause()
        dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
        if self.player != nil {
            self.player!.seek(to: kCMTimeZero)
            self.player!.play()
        }
    }
    
    // Original postmap camera view functions
    
    
    var newMedia: Bool?
    let ref = FIRDatabase.database().reference()
    let storage = FIRStorage.storage()
    let storageRef = FIRStorage.storage().reference(forURL: "gs://postmap-e0514.appspot.com/")
    let username = FIRAuth.auth()?.currentUser
    var location: CLLocation!
    var locationManager = CLLocationManager()
    var postLatitude : String = ""
    var postLongitude : String = ""
    var i = 0
    
    
    func postLocation(){
        print(location)
        if location != nil {
            print("location")
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            self.postLatitude = String(10000*latitude)
            self.postLongitude = String(10000*longitude)
            print(self.postLatitude)
        }
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.location = locations.last! as CLLocation
    }

}

//MARK: - Extension Gesture Recognizer Delegate and touch Handler for TextField

extension VideoViewController: UIGestureRecognizerDelegate {
    
    func handleTap() {
        self.textField.handleTap()
        print("TAP DETECTED")
    }
}

extension AVAsset {
    
    func videoOrientation() -> (orientation: UIInterfaceOrientation, device: AVCaptureDevicePosition) {
        var orientation: UIInterfaceOrientation = .unknown
        var device: AVCaptureDevicePosition = .unspecified
        
        let tracks :[AVAssetTrack] = self.tracks(withMediaType: AVMediaTypeVideo)
        if let videoTrack = tracks.first {
            
            let t = videoTrack.preferredTransform
            
            if (t.a == 0 && t.b == 1.0 && t.d == 0) {
                orientation = .portrait
                
                if t.c == 1.0 {
                    device = .front
                } else if t.c == -1.0 {
                    device = .back
                }
            }
            else if (t.a == 0 && t.b == -1.0 && t.d == 0) {
                orientation = .portraitUpsideDown
                
                if t.c == -1.0 {
                    device = .front
                } else if t.c == 1.0 {
                    device = .back
                }
            }
            else if (t.a == 1.0 && t.b == 0 && t.c == 0) {
                orientation = .landscapeRight
                
                if t.d == -1.0 {
                    device = .front
                } else if t.d == 1.0 {
                    device = .back
                }
            }
            else if (t.a == -1.0 && t.b == 0 && t.c == 0) {
                orientation = .landscapeLeft
                
                if t.d == 1.0 {
                    device = .front
                } else if t.d == -1.0 {
                    device = .back
                }
            }
        }
        
        return (orientation, device)
    }
}
