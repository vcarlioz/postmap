/*Copyright (c) 2016, Andrew Walz.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

//  postmap
//
//  Created by Victor Carlioz on 12/12/2016.
//  Copyright © 2016 ACLA. All rights reserved.
//

import UIKit
import SnapSliderFilters
import MobileCoreServices
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import MapKit

class PhotoViewController: UIViewController, CLLocationManagerDelegate{

    // The screenView will be the screenshoted view : all its subviews will appear on it (so, don't add buttons in its subviews)
    fileprivate let screenView:UIView = UIView(frame: CGRect(origin: CGPoint.zero, size: SNUtils.screenSize))
    fileprivate let slider:SNSlider = SNSlider(frame: CGRect(origin: CGPoint.zero, size: SNUtils.screenSize))
    fileprivate let textField = SNTextField(y: SNUtils.screenSize.height/2, width: SNUtils.screenSize.width, heightOfScreen: SNUtils.screenSize.height)
    fileprivate let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer()
    var buttonSave = SNButton(frame: CGRect(x: 20, y: SNUtils.screenSize.height - 35, width: 33, height: 30), withImageNamed: "saveButton")
    //fileprivate let buttonCamera = SNButton(frame: CGRect(x: 75, y: SNUtils.screenSize.height - 42, width: 45, height: 45), withImageNamed: "galleryButton")
    fileprivate let imagePicker = UIImagePickerController()
    fileprivate var data:[SNFilter] = []
    
    
	override var prefersStatusBarHidden: Bool {
		return true
	}

	private var backgroundImage: UIImage

	init(image: UIImage) {
		self.backgroundImage = image
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}


	override func viewDidLoad() {
		super.viewDidLoad()

		self.view.backgroundColor = UIColor.gray
		let backgroundImageView = UIImageView(frame: view.frame)
		backgroundImageView.contentMode = UIViewContentMode.scaleAspectFit
		backgroundImageView.image = backgroundImage
		//view.addSubview(backgroundImageView)
		let cancelButton = UIButton(frame: CGRect(x: 20.0, y: 10.0, width: 30.0, height: 30.0))
		cancelButton.setImage(#imageLiteral(resourceName: "cancel"), for: UIControlState())
		cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
		
        
        //snapfilters set up 
        
        tapGesture.addTarget(self, action: #selector(handleTap))
        
        setupSlider()
        setupTextField()
        view.addSubview(screenView)
        view.addSubview(cancelButton)
        setupButtonSave()
        //setupButtonCamera()
        
        // original postmap camera view viewdidload
        
        self.checkLocationAuthorizationStatus()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy=kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
	}
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(textField)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.setAnimationsEnabled(true)
    }

	func cancel() {
		dismiss(animated: true, completion: nil)
	}
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    fileprivate func setupSlider() {

        let newImage = self.resizeImage(image: backgroundImage, newWidth: 640)
        self.createData(newImage!)
        self.slider.dataSource = self
        self.slider.isUserInteractionEnabled = true
        self.slider.isMultipleTouchEnabled = true
        self.slider.isExclusiveTouch = false
        
        self.screenView.addSubview(slider)
        self.slider.reloadData()
    }
    
    fileprivate func setupTextField() {
        self.screenView.addSubview(textField)
        
        self.tapGesture.delegate = self
        self.slider.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self.textField, selector: #selector(SNTextField.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self.textField, selector: #selector(SNTextField.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self.textField, selector: #selector(SNTextField.keyboardTypeChanged(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    fileprivate func setupButtonSave() {
        self.buttonSave.setAction {
            [weak weakSelf = self] in
            let picture = SNUtils.screenShot(weakSelf?.screenView)
            if let image = picture {
                        print("PHOTOSAVE")
                
                self.postLocation()
                
                        // Data in memory
                        var data = Data()
                        let imageData: Data = UIImageJPEGRepresentation(image, 0.2)!
                        data = imageData
                        let postUIID = NSUUID.init()
                        let postUIIDString = postUIID.uuidString
                        let postTimeStamp = Timestamp
                        
                        print("time stamp :\(postTimeStamp)")
                        
                        
                        
                        // Create a reference to the file you want to upload
                        let imagesRef = self.storage.reference()
                        let postRef = imagesRef.child("images/\(postUIIDString).jpeg")
                        
                        //create post
                        
                        
                        
                        // Upload the file
                        let uploadTask = postRef.put(data, metadata: nil) { (metadata, error) in
                            guard let metadata = metadata
                                
                                else {
                                    // Uh-oh, an error occurred!
                                    return
                            }
                            
                            self.ref.child("post").childByAutoId().setValue(["postUIID": postUIIDString,"latitude":self.postLatitude,"longitude":self.postLongitude,"posted by":MyVariables.currentUserName,"time stamp":postTimeStamp])
                            
                            // Metadata contains file metadata such as size, content-type, and download URL.
                            let downloadURL = metadata.downloadURL
                            
                            //self.dismiss(animated: true, completion: nil)
                            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
  
                        }
                        DispatchQueue.main.async(execute: {
                            
                        })
            }
        }
        
        self.view.addSubview(self.buttonSave)
    }
    

    
    //MARK: Functions
    fileprivate func createData(_ image: UIImage) {
        let ImageView = UIImageView(frame: view.frame)
        ImageView.contentMode = UIViewContentMode.scaleAspectFit
        ImageView.image = image
        let correctImage = ImageView.image
        
        self.data = SNFilter.generateFilters(SNFilter(frame: self.slider.frame, withImage: correctImage!), filters: SNFilter.filterNameList)
        
        self.data[1].addSticker(SNSticker(frame: CGRect(x: 195, y: 30, width: 90, height: 90), image: UIImage(named: "stick2")!))
        self.data[2].addSticker(SNSticker(frame: CGRect(x: 30, y: 100, width: 250, height: 250), image: UIImage(named: "stick3")!))
        self.data[3].addSticker(SNSticker(frame: CGRect(x: 20, y: 00, width: 140, height: 140), image: UIImage(named: "stick")!))
    }
    
    fileprivate func updatePicture(_ newImage: UIImage) {
        createData(newImage)
        slider.reloadData()
    }
    
    // Original postmap camera view functions
    
    
    var newMedia: Bool?
    let ref = FIRDatabase.database().reference()
    let storage = FIRStorage.storage()
    let storageRef = FIRStorage.storage().reference(forURL: "gs://postmap-e0514.appspot.com/")
    let username = FIRAuth.auth()?.currentUser
    var location: CLLocation!
    var locationManager = CLLocationManager()
    var postLatitude : String = ""
    var postLongitude : String = ""
    var i = 0
    
    
    func postLocation(){
        print(location)
        if location != nil {
            print("location")
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            self.postLatitude = String(10000*latitude)
            self.postLongitude = String(10000*longitude)
            print(self.postLatitude)
        }
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.location = locations.last! as CLLocation
    }
    
}

//MARK: - Extension SNSlider DataSource

extension PhotoViewController: SNSliderDataSource {
    
    func numberOfSlides(_ slider: SNSlider) -> Int {
        return data.count
    }
    
    func slider(_ slider: SNSlider, slideAtIndex index: Int) -> SNFilter {
        
        return data[index]
    }
    
    func startAtIndex(_ slider: SNSlider) -> Int {
        return 0
    }
}

//MARK: - Extension Gesture Recognizer Delegate and touch Handler for TextField

extension PhotoViewController: UIGestureRecognizerDelegate {
    
    func handleTap() {
        self.textField.handleTap()
    }
}

// MARK: - UIImagePickerControllerDelegate Methods

extension PhotoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            // To avoid too big images and orientation issue
            let newImage = pickedImage.resizeWithWidth(SNUtils.screenSize.width)
            if let image = newImage {
                updatePicture(image)
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}
