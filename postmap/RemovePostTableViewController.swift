//
//  RemovePostTableViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 06/02/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import MapKit

class RemovePostTableViewController: UITableViewController {
    
    var postUIIDOrdered = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.retrieveOrdedered()
    }
    
    class postsClass{
        
        var postUIID : String
        var address : String
        var date : String
        var timeStamp : Float
        var mediaType : String
        var key : String
        
        
        init(postUIID : String, address : String, date : String, timeStamp : Float, mediaType : String, key : String) {
            
            self.postUIID = postUIID
            self.address = address
            self.date = date
            self.timeStamp = timeStamp
            self.mediaType = mediaType
            self.key = key
            
        }
    }
    
    var postsArray = [postsClass]()
    
    func retrieveOrdedered(){
        let refPost = FIRDatabase.database().reference().child("post")
        refPost.queryOrdered(byChild: "posted by").queryEqual(toValue: MyVariables.currentUserName).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if snapshot.exists() {
                var postPM = postsClass(postUIID: "",address: "", date: "", timeStamp: 0, mediaType: "", key : "")

                var timeStampArray = [Float]()
                for child in snapshot.children {
                    let snapchild = child as! FIRDataSnapshot
                    let value = snapchild.value as! NSDictionary
                    let key = snapchild.key
                            if let timeStampPost = value["time stamp"] as? Float {
                                if let latitudeString = value["latitude"] as? String{
                                    if let longitudeString = value ["longitude"] as? String{
                                if let postUIID = value["postUIID"] as? String {


                                        let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampPost/1000))
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "yyyy/MM/dd HH:mm"
                                        let stringDate: String = formatter.string(from: date as Date)
                                        
                                        let latitude : CLLocationDegrees = CLLocationDegrees(Float(latitudeString)!/10000)
                                        let longitude : CLLocationDegrees = CLLocationDegrees(Float(longitudeString)!/10000)
                                        
                                        var location = CLLocation(latitude: latitude, longitude: longitude)
                                        
                                       // CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in

                                            
                                           /* if error != nil {
                                                return
                                            }*/
                                            
                                            //if (placemarks?.count)! > 0 {
                                               // let pm = placemarks?[0]
                                                //if let addressPM = pm?.locality {
                                    if let mediaType = value["media type"] as? String {
                                        postPM = postsClass(postUIID: postUIID, address: " ", date: stringDate, timeStamp : timeStampPost, mediaType : "movie", key : key)
                                    }
                                    else{
                                        postPM = postsClass(postUIID: postUIID, address: " ", date: stringDate, timeStamp : timeStampPost, mediaType : "image", key : key)
                                    }
                                        
                                    
                                               // }
                                                if self.postsArray.count < 1{
                                                    self.postsArray.append(postPM)
                                                    self.tableView.reloadData()
                                                }
                                                else {
                                                for y in (0...(self.postsArray.count-1)){
                                                    if timeStampPost >= self.postsArray[y].timeStamp {
                                                        if self.postsArray.contains(where: {$0.postUIID == postPM.postUIID}){
                                                            
                                                        }
                                                        else{
                                                            self.postsArray.insert(postPM, at: y)
                                                            self.tableView.reloadData()
                                    
                                                        }
                                                    }
                                                    else{
                                                        if (y == (self.postsArray.count-1)){
                                                            self.postsArray.append(postPM)
                                                            self.tableView.reloadData()
                                                        }
                                                    }
                                                    //print(self.rankingArray.count)
                                                    
                                                }
                                            }
                                            //}
                                      //  })
                                        
                                    
                                }
                                    }
                                }
                    }
                }
            }
        })
        
        
        
        //self.downloadPosts(latitudeDown: positionLatitudeDown,latitudeUp: positionLatitudeUp,longitudeDown: longitudeRoundedDown,longitudeUp: longitudeRoundedUp)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.postsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let address = self.postsArray[indexPath.row].address
        let date = self.postsArray[indexPath.row].date
        cell.textLabel?.text = date + " " + address
        
        return cell
    }
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if tableView.cellForRow(at: indexPath) != nil {
            let postUIID = self.postsArray[indexPath.item].postUIID
            let mediaType = self.postsArray[indexPath.item].mediaType
            let keyPost = self.postsArray[indexPath.item].key
            self.postUIIDTransfered = postUIID
            self.mediaTypeTransfered = mediaType
            self.postKeyTransfered = keyPost
            
            self.performSegue(withIdentifier: "removePostSegue", sender: self)
        }
    }
    
    var postUIIDTransfered = String()
    var mediaTypeTransfered = String()
    var postKeyTransfered = String()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "removePostSegue" {
            let yourNextViewController = (segue.destination as! reviewPostViewController)
            yourNextViewController.postUIID = self.postUIIDTransfered
            yourNextViewController.mediaType = self.mediaTypeTransfered
            yourNextViewController.postKey = self.postKeyTransfered
        }
    }



    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
