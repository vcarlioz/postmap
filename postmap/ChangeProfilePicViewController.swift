//
//  ChangeProfilePicViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 11/05/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit
import MobileCoreServices
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import MapKit

class ChangeProfilePicViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var cameraView: UIImageView!

    
    var newMedia: Bool?
    let ref = FIRDatabase.database().reference()
    let storage = FIRStorage.storage()
    let storageRef = FIRStorage.storage().reference(forURL: "gs://postmap-e0514.appspot.com/")
    let username = FIRAuth.auth()?.currentUser

    var i = 0
    
    

    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    // Do any additional setup after loading the view.
    override func viewDidAppear(_ animated: Bool) {

        if self.i < 1 {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.allowsEditing = false
                imagePicker.mediaTypes = [kUTTypeImage as String]
                imagePicker.view.frame = self.view.bounds
                imagePicker.restoresFocusAfterTransition = true
                
                //imagePicker.showsCameraControls = false
                self.present(imagePicker, animated: false, completion: nil)
                
                newMedia = true
            }
        }
    }
 
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.dismiss(animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
        self.performSegue(withIdentifier: "UnWindToProfileView", sender: self)
        print("click CANCEL")
        self.i = self.i+1
        //dismiss(animated: true, completion: nil)
    }
    
    func scaleImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
     

        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        print(mediaType)
        
        self.dismiss(animated: false, completion: nil)
        
        if mediaType.isEqual(to: kUTTypeImage as String) {
            let image = info[UIImagePickerControllerOriginalImage]
                as! UIImage
            if (newMedia == true) {
                
                print("PHOTOSAVE")
                // Data in memory
                var data = Data()
                let cgImage: CGImage = image.cgImage!
                let imageScaled = self.scaleImage(image, targetSize: CGSize(width:200.0,height:200.0))
                
                let imageData: Data = UIImageJPEGRepresentation(imageScaled, 1)!
                data = imageData
                let postUIID = NSUUID.init()
                let postUIIDString = postUIID.uuidString

                
                
                
                // Create a reference to the file you want to upload
                let imagesRef = storage.reference()
                let postRef = imagesRef.child("images/\(postUIIDString).jpeg")
                
                //create post
                
                
                
                // Upload the file
                let uploadTask = postRef.put(data, metadata: nil) { (metadata, error) in
                    guard let metadata = metadata
                        
                        else {
                            // Uh-oh, an error occurred!
                            return
                    }
                    
                    self.ref.child("profilePicture").child(MyVariables.currentUserName).updateChildValues(["postUIID": postUIIDString])
                    
                    // Metadata contains file metadata such as size, content-type, and download URL.
                    let downloadURL = metadata.downloadURL
                    self.performSegue(withIdentifier: "UnWindToProfileView", sender: self)
                    self.dismiss(animated: true, completion: nil)
                }
                DispatchQueue.main.async{
                    
                }
                
                
                //UIImageWriteToSavedPhotosAlbum(image, self,#selector(CameraViewController.image(image:didFinishSavingWithError:contextInfo:)), nil)
            } /*else if mediaType.isEqual(to: kUUTypeMovie) {
             // Code to support video here
             }*/
            
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafeRawPointer) {
        
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                                          message: "Failed to save image",
                                          preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                                             style: .cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true,
                         completion: nil)
        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
