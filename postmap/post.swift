//
//  post.swift
//  postmap
//
//  Created by Victor Carlioz on 17/12/2016.
//  Copyright © 2016 ACLA. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct post {
    let postUIID : String
    let username : String
}

struct MyVariables {
    static var friendList = [String]()
    static var currentUserName = String()
    static var filteredFriendList = [String]()
    static var notificationFriendRequestCount = Int()
}

struct MyPins {
    var postBy = String()
    var longitude = Double()
    var latitude = Double()
}

struct MyMedia {
    var postTimeStamp = Float()
   // var picture = Any()
}


var Timestamp: TimeInterval {
    return NSDate().timeIntervalSince1970 * 1000
}
