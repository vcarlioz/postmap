//
//  old code.swift
//  postmap
//
//  Created by Victor Carlioz on 08/05/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import Foundation

/*func OlddisplayPosts(){
    print("display post")
    //print(self.i)
    //print(mediaArray.count)
    print(self.mediaSimplified.count)
    if self.l > 0 {
        self.postView.image = nil
        self.postView.backgroundColor = UIColor.black
        self.stopVideo()
        self.l = 0
        if self.i < self.mediaArray.count {
            
        }
        else {
            self.l = 0
        }
    }
    
    if self.mediaArray.isEmpty == false{
        self.nextPictureButton.isEnabled = true
        if self.i < self.mediaArray.count {
            
            let media = mediaArray[self.i]
            if let urlMovie = media as? URL {
                self.ActivityIndicator.stopAnimating()
                print("play video")
                self.playVideo(urlVideo: urlMovie)
                self.i = self.i + 1
                self.l = self.l + 1
                
            }
            else {
                
                if let image = self.mediaArray[self.i] as? UIImage {
                    self.ActivityIndicator.stopAnimating()
                    self.PostInfoLabel.text = self.postedByArray[self.i]
                    self.postView.image = image
                    self.i = self.i + 1
                    
                }
                else {
                    print("still downloading")
                    self.ActivityIndicator.startAnimating()
                }
            }
        }
        else {
            self.postView.backgroundColor = UIColor.clear
            self.postView.image = nil
            print ("remove post display")
            self.PostInfoLabel.text = ""
            self.nextPictureButton.isEnabled = false
            self.i = 0
        }
    }
}*/


/*func retrieveOrdedered(positionLatitudeDown: String,positionLatitudeUp: String,longitudeRoundedDown: Int,longitudeRoundedUp: Int){
    var notificationCount = 0
    let refPost = FIRDatabase.database().reference().child("post")
    refPost.queryOrdered(byChild: "latitude").queryStarting(atValue: positionLatitudeDown).queryEnding(atValue: positionLatitudeUp).observe(FIRDataEventType.value) { (snap: FIRDataSnapshot) in
        print("retrieve ordered function triggered")
        
        if snap.exists() {
            
            self.retrieveSimplified(positionLatitudeDown: positionLatitudeDown, positionLatitudeUp: positionLatitudeUp, longitudeRoundedDown: longitudeRoundedDown, longitudeRoundedUp: longitudeRoundedUp)
            
            if notificationCount == 0{
                let content = UNMutableNotificationContent()
                content.title = "Post map"
                content.body = "You just found a post!"
                content.sound = UNNotificationSound.default()
                content.badge = 2
                content.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                let request = UNNotificationRequest(identifier: "found a post notification", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                notificationCount = notificationCount + 1
            }
            
            /*self.clearTmpDirectory()
             self.resetPosts()
             
             var timeStampArray = [Float]()
             timeStampArray = []
             self.postUIIDOrdered = []
             
             for child in snap.children {
             let snapchild = child as! FIRDataSnapshot
             let value = snapchild.value as! NSDictionary
             
             if let postBy = value["posted by"] as? String {
             if MyVariables.filteredFriendList.contains(postBy) {
             if let timeStampPost = value["time stamp"] as? Float {
             var indexMedia : Int = 0
             var timeStampPostVar = timeStampPost
             if timeStampArray.count == 0{
             timeStampArray.append(timeStampPostVar)
             timeStampPostVar = 0
             }
             for y in 0...(timeStampArray.count-1){
             if timeStampPostVar >= timeStampArray[y]{
             timeStampArray.insert(timeStampPostVar, at: y)
             indexMedia = y
             timeStampPostVar = 0
             }
             }
             
             if let postUIID = value["postUIID"] as? String {
             if self.postUIIDOrdered.contains(postUIID){
             
             }
             else{
             if notificationCount == 0{
             let content = UNMutableNotificationContent()
             content.title = "Post map"
             content.body = "You just found a post!"
             content.sound = UNNotificationSound.default()
             content.badge = 2
             content.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
             let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
             let request = UNNotificationRequest(identifier: "found a post notification", content: content, trigger: trigger)
             UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
             notificationCount = notificationCount + 1
             }
             //self.mediaArray.insert("empty", at: indexMedia)
             self.postUIIDOrdered.insert(postUIID, at: indexMedia)
             self.mediaTypeArray.append("empty")
             self.mediaArray.append("empty")
             self.postedByArray.append(" ")
             self.downloadPosts(latitudeDown: positionLatitudeDown,latitudeUp: positionLatitudeUp,longitudeDown: longitudeRoundedDown,longitudeUp: longitudeRoundedUp)
             
             }
             }
             }
             }
             }
             }*/
        }
        else{
            self.ActivityIndicator.stopAnimating()
        }
    }
    
    
    
    //self.downloadPosts(latitudeDown: positionLatitudeDown,latitudeUp: positionLatitudeUp,longitudeDown: longitudeRoundedDown,longitudeUp: longitudeRoundedUp)
    
}*/



/*    func downloadPosts(latitudeDown : String,latitudeUp : String,longitudeDown : Int,longitudeUp : Int){
 self.ActivityIndicator.startAnimating()
 self.postCount = 0
 
 
 
 let refPost = FIRDatabase.database().reference().child("post")
 refPost.queryOrdered(byChild: "latitude").queryStarting(atValue: latitudeDown).queryEnding(atValue: latitudeUp).observe(FIRDataEventType.value) { (snap: FIRDataSnapshot) in
 if snap.exists() {
 
 
 for child in snap.children {
 
 let snapchild = child as! FIRDataSnapshot
 let value = snapchild.value as! NSDictionary
 
 if let longitudePost = value["longitude"] as? String {
 let longitudePostInt = Int(longitudePost)
 }
 if let postBy = value["posted by"] as? String {
 if MyVariables.filteredFriendList.contains(postBy) {
 
 
 
 if let postUIID = value["postUIID"] as? String {
 if let indexOfpostUIID = self.postUIIDOrdered.index(of: postUIID){
 if self.postUIIDArray.contains(postUIID) {
 print("already contains postUIID")
 
 if indexOfpostUIID == 0{
 // self.displayPosts()
 self.ActivityIndicator.stopAnimating()
 }
 
 }
 else {
 self.postUIIDArray.append(postUIID)
 
 
 var postMediaType : String
 
 if let postMediaType = value["media type"] as? String {
 if postMediaType == "movie" {
 print("Post movie")
 
 //self.mediaTypeArray.append("movie")
 self.mediaTypeArray[indexOfpostUIID] = "movie"
 
 let pathReference = self.storage.reference(withPath: "images/\(postUIID).mov")
 pathReference.downloadURL { url, error in
 if let error = error {
 print(error)
 } else {
 let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
 //if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((url?.lastPathComponent)!).path) {
 URLSession.shared.downloadTask(with: url!) { (location, response, error) -> Void in
 
 // use guard to unwrap your optional url
 guard let location = location else { return }
 print(location)
 
 // create a deatination url with the server response suggested file name
 let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? (url?.lastPathComponent)!)
 
 do {
 try FileManager.default.moveItem(at: location, to: destinationURL)
 
 //self.mediaArray.append(destinationURL)
 print("downloading video \(indexOfpostUIID)")
 self.postCount = self.postCount + 1
 self.mediaArray[indexOfpostUIID] = destinationURL
 self.postedByArray[indexOfpostUIID] = postBy
 
 if indexOfpostUIID == 0{
 //self.displayPosts()
 self.ActivityIndicator.stopAnimating()
 
 }
 
 
 } catch let error as NSError {
 
 print("downloading video \(indexOfpostUIID)")
 self.postCount = self.postCount + 1
 print("post Count\(self.postCount)")
 
 self.mediaArray[indexOfpostUIID] = destinationURL
 self.postedByArray[indexOfpostUIID] = postBy
 //self.postUIIDArray.append(postUIID)
 if indexOfpostUIID == 0{
 self.ActivityIndicator.stopAnimating()
 }
 
 print(error.localizedDescription)}
 
 }.resume()
 }
 }
 }
 }
 else{
 
 self.mediaTypeArray[indexOfpostUIID] = "image"
 let postMediaType = "image"
 
 let pathReference = self.storage.reference(withPath: "images/\(postUIID).jpeg")
 pathReference.downloadURL { url, error in
 if let error = error {
 print(error)
 } else {
 let sessionTask = URLSession.shared
 let task = sessionTask.dataTask(with: url!, completionHandler: { (data, respons, error) in
 
 if data != nil{
 
 print("downloading image \(indexOfpostUIID)")
 self.postCount = self.postCount + 1
 print("post Count\(self.postCount)")
 let image = UIImage(data: data!)
 //self.mediaArray.append(image!)
 self.mediaArray[indexOfpostUIID] = image!
 self.postedByArray[indexOfpostUIID] = postBy
 
 
 DispatchQueue.main.async(execute: {
 
 if indexOfpostUIID == 0{
 //self.displayPosts()
 self.ActivityIndicator.stopAnimating()
 }
 
 
 })
 }
 })
 task.resume()
 
 }
 }
 }
 }
 }
 }
 }
 }
 else {
 
 }
 }
 }
 else{
 self.ActivityIndicator.stopAnimating()
 }
 }
 
 }/*


*/*/
