//
//  reviewPostViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 11/06/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import MediaPlayer

class reviewPostViewController: UIViewController {


    @IBOutlet weak var postView: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBAction func deleteButtonAction(_ sender: Any) {
        let alert = UIAlertController(title: "Postmap",
                                      message: "Are you sure you want to delete this post?",
                                      preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes",
                                       style: .default) { action in
                                        self.deletePost()
                                        _ = self.navigationController?.popToRootViewController(animated: true)
                               
        }
        let noAction = UIAlertAction(title: "No", style: .default) { action in
            
        }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)

        
        present(alert, animated: true, completion: nil)
    }
    
    func deletePost(){
        let refPost = FIRDatabase.database().reference().child("post")
        //let key = refPost.childByAutoId().key
        print(self.postKey)
        refPost.child(self.postKey).removeValue { (error) in
            if error != nil {
                print(error)
            }
        }
        if self.mediaType == "image"{
        let imageRef = storage.reference(withPath: "images/\(self.postUIID).jpeg")
        imageRef.delete { (error) in
            if error != nil {
                // Uh-oh, an error occurred!
            } else {
                // File deleted successfully
            }
            }
        }
        else{
            let imageRef = storage.reference(withPath: "images/\(self.postUIID).mov")
            imageRef.delete { (error) in
                if error != nil {
                    // Uh-oh, an error occurred!
                } else {
                    // File deleted successfully
                }
            }
            
        }
    }
    
    let storage = FIRStorage.storage()
    
    var postUIID = String()
    var mediaType = String()
    var postKey = String()
    
    var player = AVPlayer()
    var playerLayer = AVPlayerLayer()
    
    func downloadPicture (postUIID: String, completionHandler: @escaping (_ image: UIImage) -> Void){
        
        var buffer : NSMutableData = NSMutableData()
        //self.progressView.isHidden = false
        var expectedContentLength = 0
        
        let pathReference = self.storage.reference(withPath: "images/\(postUIID).jpeg")
        pathReference.downloadURL { url, error in
            if let error = error {
                print(error)
            } else {
                let sessionTask = URLSession.shared
                let task = sessionTask.dataTask(with: url!, completionHandler: { (data, response, error) in
                    if data != nil{
                        let newimage = UIImage(data: data!)
                        completionHandler(newimage!)
                    }
                })
                task.resume()
            }
        }  
    }
    
    func downloadMovie (postUIID: String, completionHandler: @escaping (_ destinationURL: URL) -> Void){
        print("download movie")
        
        
        
        let pathReference = self.storage.reference(withPath: "images/\(postUIID).mov")
        pathReference.downloadURL { url, error in
            if let error = error {
                print(error)
            } else {
                
                let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                
                URLSession.shared.downloadTask(with: url!, completionHandler: { (location, response, error) in
                    // use guard to unwrap your optional url
                    print("movie completion started")
                    guard let location = location else { return }
                    print(location)
                    
                    // create a deatination url with the server response suggested file name
                    let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? (url?.lastPathComponent)!)
                    
                    do {
                        try FileManager.default.moveItem(at: location, to: destinationURL)
                        completionHandler(destinationURL)
                        self.playVideo(urlVideo: destinationURL)
                    } catch let error as NSError {
                        completionHandler(destinationURL)
                        print(error.localizedDescription)}
                }).resume()
                
            }
        }
    }
    
    func playVideo(urlVideo: URL) {
        
        self.player = AVPlayer(url: urlVideo)
        self.playerLayer = AVPlayerLayer(player: player)
        self.playerLayer.frame = self.view.bounds
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        self.view.layer.addSublayer(playerLayer)
        self.view.bringSubview(toFront: self.deleteButton)

        player.play()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.mediaType == "image" {
            self.downloadPicture(postUIID: self.postUIID, completionHandler: { (image) in
                DispatchQueue.main.async{
                    self.postView.image = image
                }
                
            })
        }
        else{
            self.downloadMovie(postUIID: self.postUIID, completionHandler: { (url) in
                DispatchQueue.main.async{
                    self.playVideo(urlVideo: url)
                }
            })
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.deleteButton.layer.cornerRadius = 5
        self.deleteButton.clipsToBounds = true

            
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
