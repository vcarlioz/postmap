//
//  friendRequestTableViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 19/01/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth


class friendRequestTableViewController: UITableViewController {
    
    var currentUsername = String()
    var ref: FIRDatabaseReference!
    var requestsFromArray = [String]()
    var refRequest: FIRDatabaseReference!
    var refRequestUpdate = [FIRDatabaseReference]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = FIRDatabase.database().reference()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellRequest")
        
        let userID = FIRAuth.auth()?.currentUser?.uid
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            self.currentUsername = value?["username"] as? String ?? ""
            print(self.currentUsername)
            DispatchQueue.main.async{
                self.queryRequest()
            }
        }) { (error) in
            print(error.localizedDescription)
        }
        
        
       
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func queryRequest(){
        print("username \(self.currentUsername)")
        self.refRequest = FIRDatabase.database().reference().child("friend requests")
        
        refRequest.queryOrdered(byChild: "sent to").queryEqual(toValue: self.currentUsername).observeSingleEvent(of: FIRDataEventType.value) { (snap: FIRDataSnapshot) in
            print(snap)
            if snap.exists() {
                print("snap exist")
                
                for child in snap.children {
                    let snapchild = child as! FIRDataSnapshot
                    let postkey = snapchild.key
                    let value = snapchild.value as! NSDictionary
                    if let statusRequest = value["status"] as? String{
                        if statusRequest == "pending" {
                    if let usernameRequest = value["request from"] as? String{
                        print(usernameRequest)
                        self.requestsFromArray.append(usernameRequest)
                        self.refRequestUpdate.append(FIRDatabase.database().reference().child("friend requests").child(postkey))
                    }
                        }
                    }
                }
            }
            DispatchQueue.main.async{
                self.tableView.reloadData()
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return self.requestsFromArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellRequest", for: indexPath)

        let usernameRequest = self.requestsFromArray[indexPath.row]
        cell.textLabel?.text = usernameRequest
        return cell
    }
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("select row")
        let requestSelected = self.requestsFromArray[indexPath.row]
        
        let alert = UIAlertController(title: "Postmap",
                                      message: "Accept friend request to \(requestSelected)?",
            preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "yes",
                                      style: .default){ action in
                                        self.ref.child("friends").childByAutoId().setValue(["friend1": self.currentUsername,"friend2": requestSelected])
                                        self.refRequestUpdate[indexPath.row].updateChildValues(["status" : "accepted"])
                                        self.requestsFromArray.remove(object: requestSelected)
                                        if MyVariables.notificationFriendRequestCount > 0 {
                                           MyVariables.notificationFriendRequestCount = MyVariables.notificationFriendRequestCount - 1
                                        }
                                        self.tableView.reloadData()
                                        
 
                                        
        }
        let noAction = UIAlertAction(title: "no",
                                     style: .default){ action in
                                        self.refRequestUpdate[indexPath.row].updateChildValues(["status" : "refused"])
                                        self.requestsFromArray.remove(object: requestSelected)
                                        if MyVariables.notificationFriendRequestCount > 0 {
                                            MyVariables.notificationFriendRequestCount = MyVariables.notificationFriendRequestCount - 1
                                        }
                                        self.tableView.reloadData()
        }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        present(alert, animated: true, completion: nil)
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
