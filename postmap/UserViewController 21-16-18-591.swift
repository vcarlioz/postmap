//
//  UserViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 15/01/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth


class UserViewController: UIViewController, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet var userTableView: UITableView!
    
    var searchController = UISearchController()
    
    
    var ref: FIRDatabaseReference!
    var usernamesArray = [String]()
    var currentUsername = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = FIRDatabase.database().reference()
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        //Set delegate
        searchController.searchResultsUpdater = self
        //Add to top of table view
        userTableView.tableHeaderView = searchController.searchBar
        
        userTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        userTableView.separatorStyle = .none
        
        let userID = FIRAuth.auth()?.currentUser?.uid
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            self.currentUsername = value?["username"] as? String ?? ""
        }) { (error) in
            print(error.localizedDescription)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        //        return Business.count
        return 1
    }
    
    func userTableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        print("number of row")
        return 4
    }
    
    func userTableView(tableView: UITableView?, cellForRowAtIndexPath indexPath: NSIndexPath?) -> UITableViewCell? {
        let cell = tableView!.dequeueReusableCell(withIdentifier: "cell", for: indexPath! as IndexPath)
        print("usertableview")
        if let path = indexPath {
            let username = self.usernamesArray[path.section]
            cell.textLabel?.text = username
        }
        return cell
    }
    
    
    
    func findUsers(text: String)->Void{
        ref.child("users").queryOrdered(byChild: "username").queryStarting(atValue: text).queryEnding(atValue: text+"\u{f8ff}").observe(.value, with: { snapshot in
        //ref.child("users").queryOrdered(byChild: "username").queryEqual(toValue: text).observeSingleEvent(of: FIRDataEventType.value) { (snapshot: FIRDataSnapshot) in
            if snapshot.exists() {
                print("snap exist")
            for child in snapshot.children {
                let snapchild = child as! FIRDataSnapshot
                let value = snapchild.value as! NSDictionary
                if let username = value["username"] as? String {
                    print(username)
                    if self.usernamesArray.contains(username){
                        
                    }
                    else {
                             self.usernamesArray.append(username)
                        print(self.usernamesArray)
                        DispatchQueue.main.async{
                          self.userTableView.reloadData()
                            print("dispatched")
                        }
                        
                    }
                }
            }
            }

        })
        DispatchQueue.main.async{
            self.userTableView.reloadData()
            print("dispatched")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        if searchController.searchBar.text == "" {

        }else
        {
            print("not empty")
            self.usernamesArray = []
     self.findUsers(text: searchController.searchBar.text!)

        }
    }
}
