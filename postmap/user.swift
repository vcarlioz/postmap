//
//  user.swift
//  postmap
//
//  Created by Victor Carlioz on 10/12/2016.
//  Copyright © 2016 ACLA. All rights reserved.
//

import Foundation
import FirebaseAuth

struct User {
    
    let uid: String
    let email: String

    
    init(authData: FIRUser) {
        uid = authData.uid
        email = authData.email!

    }
    
    init(uid: String, email: String, username: String) {
        self.uid = uid
        self.email = email

    }
}

