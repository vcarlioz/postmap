//
//  ProfileViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 18/01/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class ProfileViewController: UIViewController {
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var addFriendButton: UIButton!
    @IBOutlet weak var removeFriendButton: UIButton!
    @IBOutlet weak var friendRequestsButton: UIButton!
    @IBOutlet weak var removePostsButton: UIButton!

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var friendRequestsCountLabel: UILabel!
    
    let storage = FIRStorage.storage()
    
    @IBOutlet weak var editProfilePic: UIButton!
    @IBOutlet weak var nbOfPostsLabel: UILabel!
    
    /*@IBAction func changeProfilePic(_ sender: Any) {
     
    }*/
    var ref: FIRDatabaseReference!
    var postCount : Int = 0
    
    
    @IBAction func UnWindToProfileViewController (sender:UIStoryboardSegue){
        print("unwind segue?")
    }
    
    func numbOfPost (username : String){
        let refPost = FIRDatabase.database().reference().child("post")
        refPost.queryOrdered(byChild: "posted by").queryEqual(toValue: username).observeSingleEvent(of: FIRDataEventType.value, with: { (snap) in
            if !snap.exists() {
                
            }
            else {
                self.postCount = Int(snap.childrenCount)
                DispatchQueue.main.async {
                self.nbOfPostsLabel.text = "\(self.postCount) posts"
                }
            }
        })
    }
    
    func downloadProfilePic (username : String){
        
       
        
        let refProfilePic = FIRDatabase.database().reference().child("profilePicture")
        refProfilePic.child(username).observeSingleEvent(of: .value) { (snap : FIRDataSnapshot) in
            print ("download PIC FOR \(username)")
            
            if !snap.exists() {
                print("NO SNAP!")
                print ("FOR \(username)")
                return
            }
            let value = snap.value as! NSDictionary
            if let postUIID = value["postUIID"] as? String{
                let pathReference = self.storage.reference(withPath: "images/\(postUIID).jpeg")
                
                pathReference.downloadURL { url, error in
                    if let error = error {
                        print(error)
                    } else {
                        let sessionTask = URLSession.shared
                        let task = sessionTask.dataTask(with: url!, completionHandler: { (data, response, error) in
                            if data != nil{
                                 print("DOWNLOAD PROFILE PIC")
                                    let image = UIImage(data: data!)
                                DispatchQueue.main.async {
                                    self.profilePic.image = image
                                    self.view.bringSubview(toFront: self.editProfilePic)
                                }
                            }
                        })
                        task.resume()
                        self.view.reloadInputViews()
                        
                    }
                }
                
                
            }
            
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profilePic.clipsToBounds = true
        self.profilePic.layer.cornerRadius = 49.5
        self.profilePic.layer.borderWidth = 1
        self.profilePic.layer.borderColor = UIColor(red: 245/255, green: 78/255, blue: 188/255, alpha: 1).cgColor
        
        self.editProfilePic.layer.cornerRadius = 2
        self.editProfilePic.layer.borderWidth = 1
        self.editProfilePic.layer.borderColor = UIColor.lightGray.cgColor
        self.addFriendButton.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        self.removeFriendButton.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        self.friendRequestsButton.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        self.removePostsButton.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)

        let username = MyVariables.currentUserName
        self.usernameLabel.text = username
        
       
        self.friendRequestsCountLabel.layer.cornerRadius = 7.5
        self.friendRequestsCountLabel.clipsToBounds = true
        self.friendRequestsCountLabel.isHidden = true

        

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if MyVariables.notificationFriendRequestCount < 1 {
            self.friendRequestsCountLabel.isHidden = true
        }
        else {
            self.friendRequestsCountLabel.text = String(MyVariables.notificationFriendRequestCount)
            self.friendRequestsCountLabel.isHidden = false
        }
        let username = MyVariables.currentUserName
        self.downloadProfilePic(username: username)
        self.numbOfPost(username: username)
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Sets background to a blank/empty image
       // UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
       // UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        
        UINavigationBar.appearance().backgroundColor = UIColor.darkGray.withAlphaComponent(0.6)
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        //UINavigationBar.appearance().isTranslucent = true
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
