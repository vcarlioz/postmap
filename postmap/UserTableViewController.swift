//
//  UserTableViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 15/01/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth


class UserTableViewController: UITableViewController, UISearchResultsUpdating {
    
    var ref: FIRDatabaseReference!
    var usernamesArray = [String]()
    var searchController = UISearchController()
    var currentUsername = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        definesPresentationContext = true
        self.ref = FIRDatabase.database().reference()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        self.searchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        let userID = FIRAuth.auth()?.currentUser?.uid
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            self.currentUsername = value?["username"] as? String ?? ""
        }) { (error) in
            print(error.localizedDescription)
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.usernamesArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
            let username = self.usernamesArray[indexPath.row]
            cell.textLabel?.text = username
        return cell
    }
    
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("select row")
        let usernameSelected = self.usernamesArray[indexPath.row]
        
        if MyVariables.friendList.contains(usernameSelected){
            let alert = UIAlertController(title: "Postmap",
                                          message: "You are already friend with \(usernameSelected)",
                preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ok",
                                         style: .default){action in
                                            self.searchController.isActive = false
            }
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)

        }
            
        else {

        let alert = UIAlertController(title: "Postmap",
                                      message: "Send friend request to \(usernameSelected)?",
                                      preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "yes",
                                         style: .default){ action in
                    self.ref.child("friend requests").childByAutoId().setValue(["request from": self.currentUsername,"sent to": usernameSelected,"status": "pending"])
                    self.searchController.isActive = false
                                            
        }
        let noAction = UIAlertAction(title: "no",
                                     style: .default){action in
                                        self.searchController.isActive = false
            }
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        present(alert, animated: true, completion: nil)
        }
        
    }
    
    

    func updateSearchResults(for searchController: UISearchController) {
        
        if searchController.searchBar.text == "" {
            self.usernamesArray = []
            self.tableView.reloadData()
        }
        else
        {
            print("not empty")
            self.usernamesArray = []
            self.findUsers(text: searchController.searchBar.text!)
            
        }
    }
    
    func findUsers(text: String)->Void{
        
        
        ref.child("users").queryOrdered(byChild: "username").queryStarting(atValue: text).queryEnding(atValue: text+"\u{f8ff}").observe(.value, with: { snapshot in
            if snapshot.exists() {
                print("snap exist")
                for child in snapshot.children {
                    let snapchild = child as! FIRDataSnapshot
                    let value = snapchild.value as! NSDictionary
                    if let username = value["username"] as? String {
                        print(username)
                        if self.usernamesArray.contains(username){
                            
                        }
                        else {
                            self.usernamesArray.append(username)
                            print(self.usernamesArray)
                            DispatchQueue.main.async{
                                self.tableView.reloadData()
                                print("dispatched")
                            }
                            
                        }
                    }
                }
            }
            
        })
        let textLowerCase = text.lowercased()
        
        ref.child("users").queryOrdered(byChild: "username").queryStarting(atValue: textLowerCase).queryEnding(atValue: textLowerCase+"\u{f8ff}").observe(.value, with: { snapshot in
            if snapshot.exists() {
                print("snap exist")
                for child in snapshot.children {
                    let snapchild = child as! FIRDataSnapshot
                    let value = snapchild.value as! NSDictionary
                    if let username = value["username"] as? String {
                        print(username)
                        if self.usernamesArray.contains(username){
                            
                        }
                        else {
                            self.usernamesArray.append(username)
                            print(self.usernamesArray)
                            DispatchQueue.main.async{
                                self.tableView.reloadData()
                                print("dispatched")
                            }
                            
                        }
                    }
                }
            }
            
        })
        
        
        DispatchQueue.main.async{
            self.tableView.reloadData()
            print("dispatched")
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}

