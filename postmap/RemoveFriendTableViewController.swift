//
//  RemoveFriendTableViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 22/01/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class RemoveFriendTableViewController: UITableViewController, UISearchResultsUpdating {
    
    var searchController = UISearchController()
    var searchFriendArray = [String]()
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if searchController.searchBar.text == "" {
            self.searchFriendArray = MyVariables.friendList
            self.tableView.reloadData()
        }
        else
        {
            let searchString = searchController.searchBar.text
            self.searchFriendArray = MyVariables.friendList.filter({ (friend) -> Bool in
                print(self.searchFriendArray)
                let friendText = friend as String
                return ((friendText.range(of: searchString!, options: String.CompareOptions.caseInsensitive) != nil))
            })
            //self.usernamesArray = []
            // self.findUsers(text: searchController.searchBar.text!)
            self.tableView.reloadData()
        }
    }
    
    func findFriends() {
        
        let refFriends = FIRDatabase.database().reference().child("friends")
        refFriends.queryOrdered(byChild: "friend1").queryEqual(toValue: MyVariables.currentUserName).observeSingleEvent(of: FIRDataEventType.value) { (snap: FIRDataSnapshot) in
            print(snap)
            if snap.exists() {
                for child in snap.children {
                    let snapchild = child as! FIRDataSnapshot
                    let value = snapchild.value as! NSDictionary
                    if let friendUsername = value["friend2"] as? String{
                        if MyVariables.friendList.contains(friendUsername){
                            
                        }
                        else {
                            MyVariables.friendList.append(friendUsername)
                            MyVariables.filteredFriendList.append(friendUsername)
                        }
                    }
                }
            }
            DispatchQueue.main.async{
                refFriends.queryOrdered(byChild: "friend2").queryEqual(toValue: MyVariables.currentUserName).observeSingleEvent(of: FIRDataEventType.value) { (snap: FIRDataSnapshot) in
                    if snap.exists() {

                        
                        for child in snap.children {
                            let snapchild = child as! FIRDataSnapshot
                            let value = snapchild.value as! NSDictionary
                            if let friendUsername = value["friend1"] as? String{
                                if MyVariables.friendList.contains(friendUsername){
                                    
                                }
                                else {
                                    MyVariables.friendList.append(friendUsername)
                                    MyVariables.filteredFriendList.append(friendUsername)
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async{
                        self.searchFriendArray = MyVariables.friendList
                        self.tableView.reloadData()
                    }
                }
                
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.findFriends()
        
        self.searchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")


        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.searchFriendArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let friendname = self.searchFriendArray[indexPath.row]
        cell.textLabel?.text = friendname

        return cell
    }
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if let cell = tableView.cellForRow(at: indexPath) {
            if let usernameSelected = cell.textLabel?.text{
                if usernameSelected == MyVariables.currentUserName {
                    let alert = UIAlertController(title: "Postmap",
                                                  message: "You cannot remove yourself",
                        preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "ok",
                                                 style: .default)
                    
                    alert.addAction(okAction)
                    present(alert, animated: true, completion: nil)
                }
                else {

            let alert = UIAlertController(title: "Postmap",
                                          message: "Remove \(usernameSelected) from your friends?",
                preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "yes",
                                          style: .default){ action in
                let refFriends = FIRDatabase.database().reference().child("friends")
                refFriends.queryOrdered(byChild: "friend1").queryEqual(toValue: MyVariables.currentUserName).observeSingleEvent(of: FIRDataEventType.value) { (snap: FIRDataSnapshot) in
                    if snap.exists() {
                        for child in snap.children {
                            let snapchild = child as! FIRDataSnapshot
                            print("snapchild 1st query")
                            let value = snapchild.value as! NSDictionary
                            if let friendUsername = value["friend2"] as? String{
                                print("friendUsername :\(friendUsername)")
                                print("userNameSelected :\(usernameSelected)")
                                if friendUsername == usernameSelected {
                                    snapchild.ref.removeValue()
                                    print("remove value at friend 1")
                                }
                        
                            }
                        }
                                                    
                    }
                }
                                            
            refFriends.queryOrdered(byChild: "friend2").queryEqual(toValue: MyVariables.currentUserName).observeSingleEvent(of: FIRDataEventType.value) { (snap: FIRDataSnapshot) in
                    if snap.exists() {
                        for child in snap.children {
                                let snapchild = child as! FIRDataSnapshot
                                print("snapchild 1st query")
                                print(snapchild)
                                    let value = snapchild.value as! NSDictionary
                                        if let friendUsername = value["friend1"] as? String{
                                            print("friendUsername :\(friendUsername)")
                                            print("userNameSelected :\(usernameSelected)")
                                            if friendUsername == usernameSelected {
                                                snapchild.ref.removeValue()
                                                print("remove value at friend 2")
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                            }
                                            
            DispatchQueue.main.async{
                MyVariables.filteredFriendList.remove(object: usernameSelected)
                MyVariables.friendList.remove(object: usernameSelected)
                self.searchFriendArray.remove(object: usernameSelected)
                
                self.tableView.reloadData()
                    }
            }
            let noAction = UIAlertAction(title: "no",
                                         style: .default)
            
            alert.addAction(yesAction)
            alert.addAction(noAction)
            present(alert, animated: true, completion: nil)
            }
        }
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
