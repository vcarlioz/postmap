//
//  ProfileCollectionViewCell.swift
//  postmap
//
//  Created by Victor Carlioz on 10/05/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var initialLabel: UILabel!

}
