//
//  FilterTableViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 21/01/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import UIKit

class FilterTableViewController: UITableViewController, UISearchResultsUpdating {
    
    var searchController = UISearchController()
    var checked = [Bool]()
    var searchFriendArray = [String]()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        if MyVariables.filteredFriendList.isEmpty{
            MyVariables.filteredFriendList = MyVariables.friendList
        }
        
        self.searchFriendArray = MyVariables.friendList
        
        self.searchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if let cell = tableView.cellForRow(at: indexPath) {
            if cell.accessoryType == .checkmark {
                if MyVariables.filteredFriendList.count > 1 {
                cell.accessoryType = .none
                MyVariables.filteredFriendList.remove(object: (cell.textLabel?.text)!)
                print(MyVariables.filteredFriendList)
                }

            } else {
                cell.accessoryType = .checkmark
                MyVariables.filteredFriendList.append((cell.textLabel?.text)!)
                print(MyVariables.filteredFriendList)
            }
        }

    }
    
    /*func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            //vcell.accessoryType = .none
        }
    }*/
    


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //return MyVariables.friendList.count
        return self.searchFriendArray.count
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if searchController.searchBar.text == "" {
            self.searchFriendArray = MyVariables.friendList
            self.tableView.reloadData()
        }
        else
        {
            let searchString = searchController.searchBar.text
            self.searchFriendArray = MyVariables.friendList.filter({ (friend) -> Bool in
                print(self.searchFriendArray)
                let friendText = friend as String
                return ((friendText.range(of: searchString!, options: String.CompareOptions.caseInsensitive) != nil))
            })
            //self.usernamesArray = []
           // self.findUsers(text: searchController.searchBar.text!)
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
        /*let friendname = MyVariables.friendList[indexPath.row]
        cell.textLabel?.text = friendname
        if MyVariables.filteredFriendList.contains(friendname){
           cell.accessoryType = .checkmark
        }
        tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableViewScrollPosition.bottom)
        cell.selectionStyle = .gray
        return cell*/
        
        let friendname = self.searchFriendArray[indexPath.row]
        cell.textLabel?.text = friendname
        
        if MyVariables.filteredFriendList.contains(friendname){
            cell.accessoryType = .checkmark
        }
        else {
            cell.accessoryType = .none
        }

        return cell
    }
    

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}
