//
//  LoginViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 10/12/2016.
//  Copyright © 2016 ACLA. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class LoginViewController: UIViewController {
    
    var ref: FIRDatabaseReference!
    
    let loginToMainMenu = "LoginToMainMenu"
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passcodeTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    
    @IBAction func logInDidTouch(_ sender: UIButton) {
        FIRAuth.auth()!.signIn(withEmail: emailTextField.text!,
                               password: passcodeTextField.text!)
    }
    @IBAction func SignupDidTouch(_ sender: UIButton) {
        let alert = UIAlertController(title: "Postmap",
                                      message: "Please create a new account",
                                      preferredStyle: .alert)
        
        
        
 
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default) { action in
                                        
               
                
                    let emailField = alert.textFields![0]
                    let passwordField = alert.textFields![1]
                    let username = alert.textFields![2]
                
                        //if passwordField.text == passwordFieldVerification.text {
                            
                FIRAuth.auth()!.createUser(withEmail: emailField.text!,password: passwordField.text!)
                { user, error in
                    print(error)
                    if error == nil {
                        print("GOOO")
                        FIRAuth.auth()!.signIn(withEmail: self.emailTextField.text!,password: self.passcodeTextField.text!)
                        self.ref.child("users").child((user?.uid)!).setValue(["username": username.text!])
                        let changeRequest = FIRAuth.auth()?.currentUser?.profileChangeRequest()
                        changeRequest?.displayName = username.text!
                        changeRequest?.commitChanges() { (error) in
                            // ...
                        }
                    }
                    //}
               }
            /*else{
            let alertPassword = UIAlertController(title: "passwords don't match", message: "", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ok",style: .default)
            self.present(alertPassword, animated: true, completion: nil)
            alertPassword.addAction(okAction)
            }*/
           
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default)
        
        alert.addTextField { textEmail in
            textEmail.placeholder = "email"
        }
        
        alert.addTextField { textPassword in
            textPassword.isSecureTextEntry = true
            textPassword.placeholder = "password"
        }
        
        alert.addTextField { textusername in
            textusername.placeholder = "choose username"
        }
        
        /*func passwordValidation(_ sender: Any){
            let textPasswordVerif = sender as! UITextField
            let textPassword = alert.textFields![1]
            var resp : UIResponder! = textPasswordVerif
            while !(resp is UIAlertController){resp = resp.next}
            let alert = resp as! UIAlertController
            alert.actions[1].isEnabled = (textPasswordVerif == textPassword)
        }*/

        
       /*alert.addTextField { textPasswordVerif in
            textPasswordVerif.isSecureTextEntry = true
            textPasswordVerif.placeholder = "Verify your password"
            //textPasswordVerif.addTarget(self, action: Selector(("passwordValidation:")), for: .editingChanged)
        }*/
        
        
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        //saveAction.isEnabled = false
        
        present(alert, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
 
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.ref = FIRDatabase.database().reference()

        self.loginButton.backgroundColor = UIColor(white: 1, alpha: 0.4)
        self.signupButton.backgroundColor = UIColor(white: 1, alpha: 0.4)
        emailTextField.placeholder = "email"
        passcodeTextField.placeholder = "password"
        passcodeTextField.isSecureTextEntry = true
        
        FIRAuth.auth()!.addStateDidChangeListener() { auth, user in
            // 2
            if user != nil {

                // 3
                print("login \(user)")
                self.performSegue(withIdentifier: self.loginToMainMenu, sender: nil)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
