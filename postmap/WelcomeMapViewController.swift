//
//  WelcomeMapViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 11/12/2016.
//  Copyright © 2016 ACLA. All rights reserved.
//

import UIKit
import FirebaseAuth
import MapKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import MediaPlayer
import AVKit
import AVFoundation
import UserNotifications




class WelcomeMapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, NVActivityIndicatorViewable, URLSessionDelegate, URLSessionDataDelegate, UICollectionViewDelegate, UICollectionViewDataSource {


    

    @IBOutlet weak var notificationCountLabel: UILabel!
    
    @IBOutlet weak var designLabel1: UILabel!
    
    @IBOutlet weak var nextPictureButton: UIButton!
    
    @IBAction func nextPicture(sender: UIButton) {
    self.ActivityIndicatorCustom2.stopAnimating()
    self.displayPosts()
    }
    @IBOutlet weak var previousPictureButton: UIButton!
    
    
    @IBAction func previousPicture(_ sender: Any) {
        self.ActivityIndicatorCustom2.stopAnimating()
        if self.i < 2 {
            
                self.postView.backgroundColor = UIColor.clear
                self.postView.image = nil
                self.StoryCollection.isHidden = false
                self.PostInfoLabel.text = ""
                self.postDate.text = ""
                self.nextPictureButton.isEnabled = false
                self.previousPictureButton.isEnabled = false
                self.removeSwipe()
                self.i = 0
            if self.l > 0 {
                self.stopVideo()
            }
            
        }
        else{
            self.i = self.i - 2
            if self.storySelected == self.mediaSimplified[self.i].postedBy {
                self.displayPosts()
            }
            else{
                self.i = self.i - 1
                self.previousPicture(self)
            }

        }
    }
    
    @IBOutlet weak var welcomeMapView: MKMapView!
    @IBAction func postButton(_ sender: Any) {
        self.performSegue(withIdentifier: "cameraButtonSegue", sender: nil)
    }
    @IBOutlet weak var postView: UIImageView!
    
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    
 
    
    @IBOutlet weak var ActivityIndicatorCustom2: NVActivityIndicatorView!
    
    @IBOutlet weak var postDate: UILabel!
    
    @IBOutlet weak var StoryCollection: UICollectionView!
    
    
    @IBOutlet weak var PostInfoLabel: UILabel!
    var user: User!
    var location: CLLocation!
    var locationManager = CLLocationManager()
    let ref = FIRDatabase.database().reference()
    var postLatitude :  String!
    var postLongitude : String!
    var positionLatitude :  String!
    var positionLongitude : String!
    var postPath : String!
    var i = 0
    var j = 0
    var k = 0
    var l = 0
    var m = 0
    var previousLatitude : Double = 0
    var previousLongitude : Double = 0
    var postCount = 0
    var notificationNumber : Int = 0
    
    var pinArrayLatitude = [Double]()
    var pinArrayLongitude = [Double]()
    var postUIIDArray = [String]()
    var urlImagesArray = [URL]()
    let storage = FIRStorage.storage()
    var mediaTypeArray = [String]()
    var mediaArray = [Any]()
    var postedByArray = [String]()
    var postedByDateArray = [NSDate]()
    var pinsArray = [MyPins]()
    var postUIIDOrdered = [String]()
    
    typealias CompletionHandlerRefreshLocation = (_ successLocation:Bool) -> Void
    typealias CompletionHandlerRetrievePostUIID = (_ success:Bool) -> Void
    typealias CompletionHandlerRetrievePostImage = (_ success:Bool) -> Void
    
    let activityData = ActivityData()
    
    
    @IBAction func refreshLocationButton(_ sender: UIButton) {
        self.j = 0
        self.refreshLocation()
    }
    

    func clearTmpDirectory() {
        do {
            let tmpDirectory = try FileManager.default.contentsOfDirectory(atPath: NSTemporaryDirectory())
            try tmpDirectory.forEach { file in
                let path = String.init(format: "%@%@", NSTemporaryDirectory(), file)
                try FileManager.default.removeItem(atPath: path)
            }
        } catch {
            print(error)
        }
    }
    
    func refreshLocation(){
        
        if self.j < 1 {
        if location != nil {

            
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        
                let latitude = self.location.coordinate.latitude
                let latitudeRounded = Int(Double(round(10000*latitude)))
                let longitude = self.location.coordinate.longitude
                let longitudeRounded = Int(Double(round(10000*longitude)))
                self.postLatitude = String(latitudeRounded)
                self.postLongitude = String(longitudeRounded)
            
                self.welcomeMapView.setRegion(region, animated: true)
            
                self.postPath = "post/\(self.postLatitude!)/\(self.postLongitude!)"
                let currentPostRef = FIRDatabase.database().reference(withPath: self.postPath)

            if self.pinsArray.isEmpty == false{
                var i = 0
                self.j = self.j + 1

            }
            }
        }
    }
    

    func retrievePostUIIDBackground(){
        
        if location != nil {
            
            let precisionSearch = 3 as Double
            
            let latitude = location.coordinate.latitude
            let latitudeRounded = Double(10000*latitude)
            let latitudeRoundedUp = latitudeRounded + precisionSearch
            let latitudeRoundedDown = latitudeRounded - precisionSearch
            let longitude = location.coordinate.longitude
            let longitudeRounded = Double(10000*longitude)
            let longitudeRoundedUp = longitudeRounded + precisionSearch
            let longitudeRoundedDown = longitudeRounded - precisionSearch
            self.positionLatitude = String(latitudeRounded)
            let positionLatitudeUp = String(latitudeRoundedUp)
            let positionLatitudeDown = String(latitudeRoundedDown)
            
            self.positionLongitude = String(longitudeRounded)
            
            self.foundPostNotif(positionLatitudeDown: positionLatitudeDown, positionLatitudeUp: positionLatitudeUp, longitudeRoundedDown: longitudeRoundedDown, longitudeRoundedUp: longitudeRoundedUp)
            
        }
    }
    
    

    
    func retrievePostUIID(){

        

        if location != nil {
            
            let precisionSearch = 3 as Double

        let latitude = location.coordinate.latitude
        let latitudeRounded = Double(10000*latitude)
        let latitudeRoundedUp = latitudeRounded + precisionSearch
        let latitudeRoundedDown = latitudeRounded - precisionSearch
        let longitude = location.coordinate.longitude
        let longitudeRounded = Double(10000*longitude)
        let longitudeRoundedUp = longitudeRounded + precisionSearch
        let longitudeRoundedDown = longitudeRounded - precisionSearch
        self.positionLatitude = String(latitudeRounded)
        let positionLatitudeUp = String(latitudeRoundedUp)
        let positionLatitudeDown = String(latitudeRoundedDown)

        self.positionLongitude = String(longitudeRounded)

            self.retrieveSimplified(positionLatitudeDown: positionLatitudeDown, positionLatitudeUp: positionLatitudeUp, longitudeRoundedDown: longitudeRoundedDown, longitudeRoundedUp: longitudeRoundedUp)
            
        }
    }
    func downloadPicture (postUIID: String, completionHandler: @escaping (_ image: UIImage) -> Void){
        
        var buffer : NSMutableData = NSMutableData()

        var expectedContentLength = 0
    
        let pathReference = self.storage.reference(withPath: "images/\(postUIID).jpeg")
        pathReference.downloadURL { url, error in
            if let error = error {
                print(error)
            } else {
                let sessionTask = URLSession.shared
                let task = sessionTask.dataTask(with: url!, completionHandler: { (data, response, error) in
                   if data != nil{
                        let image = UIImage(data: data!)
                        completionHandler(image!)
                    }
                })
                task.resume()
            }
        }
        
        
    }
    
    func downloadMovie (postUIID: String, completionHandler: @escaping (_ destinationURL: URL) -> Void){
        let pathReference = self.storage.reference(withPath: "images/\(postUIID).mov")
        pathReference.downloadURL { url, error in
            if let error = error {
                print(error)
            } else {

                let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                
                URLSession.shared.downloadTask(with: url!, completionHandler: { (location, response, error) in
                    // use guard to unwrap your optional url
                    guard let location = location else { return }

                    
                    // create a deatination url with the server response suggested file name
                    let destinationURL = documentsDirectoryURL.appendingPathComponent(response?.suggestedFilename ?? (url?.lastPathComponent)!)
                    
                    do {
                        try FileManager.default.moveItem(at: location, to: destinationURL)
                        
                        completionHandler(destinationURL)
                    
                        
                    } catch let error as NSError {
                        completionHandler(destinationURL)
                    }
                }).resume()

            }
        }
        }
    
    var notificationCount = 0
    
    func foundPostNotif(positionLatitudeDown: String,positionLatitudeUp: String,longitudeRoundedDown: Double,longitudeRoundedUp: Double){
        
        self.refPost.queryOrdered(byChild: "latitude").queryStarting(atValue: positionLatitudeDown).queryEnding(atValue: positionLatitudeUp).observe(FIRDataEventType.value) { (snap: FIRDataSnapshot) in
            if snap.exists(){
                for child in snap.children {
                let snapchild = child as! FIRDataSnapshot
                let value = snapchild.value as! NSDictionary
                
                if let longitude = value["longitude"] as? String {
                    let longitudeDouble = Double(longitude)
                    if longitudeDouble! >= longitudeRoundedDown {
                        if longitudeDouble! <= longitudeRoundedUp {
        if self.notificationCount == 0{
            let content = UNMutableNotificationContent()
            content.title = "Post map"
            content.body = "You just found a post!"
            content.sound = UNNotificationSound.default()
            content.badge = 2
            content.setValue(true, forKey: "shouldAlwaysAlertWhileAppIsForeground")
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 30, repeats: false)
            let request = UNNotificationRequest(identifier: "found a post notification", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            self.notificationCount = self.notificationCount + 1
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    

    class media{
        var mediaType : String
        var postUIID : String
        var content : Any
        var timeStamp : Float
        var postedBy : String
        var date : String
        
        init(mediaType:String, postUIID : String, content : Any, timeStamp : Float, postedBy : String, date : String) {
            
            self.mediaType = mediaType
            self.postUIID = postUIID
            self.postedBy = postedBy
            self.content = content
            self.timeStamp = timeStamp
            self.date = date
            
        }

    }
    
    class rankers{
        var timeStamp : Float
        var postUIID : String
        var postBy : String
        init(timeStamp : Float, postUIID : String, postBy : String) {
            self.timeStamp = timeStamp
            self.postUIID = postUIID
            self.postBy = postBy
        }
    }

    var mediaSimplified = [media]()
    let refPost = FIRDatabase.database().reference().child("post")
    var timeStampArray = [Float]()
    var rankingArray = [rankers]()
    
    
    
    func retrieveSimplified (positionLatitudeDown: String,positionLatitudeUp: String,longitudeRoundedDown: Double,longitudeRoundedUp: Double){

        self.timeStampArray = [0]
        var postMedia = media(mediaType: "", postUIID: "", content: "", timeStamp: 0, postedBy: "", date: "")
        var postRanker = rankers(timeStamp: 0,postUIID: "", postBy: "")
        
        self.mediaSimplified = []
        self.rankingArray = []
        
        
        self.refPost.queryOrdered(byChild: "latitude").queryStarting(atValue: positionLatitudeDown).queryEnding(atValue: positionLatitudeUp).observe(FIRDataEventType.value) { (snap: FIRDataSnapshot) in
           
            //ranking PostUIID with time stamp
            if snap.exists() {
                DispatchQueue.main.async{
                }


                for child in snap.children {
                    let snapchild = child as! FIRDataSnapshot
                    let value = snapchild.value as! NSDictionary
                    
                    if let longitude = value["longitude"] as? String {
                        let longitudeDouble = Double(longitude)
                        if longitudeDouble! >= longitudeRoundedDown {
                            if longitudeDouble! <= longitudeRoundedUp {
                    
                    if let postBy = value["posted by"] as? String {
                        if MyVariables.filteredFriendList.contains(postBy) {
                            if let postUIID = value["postUIID"] as? String {

                    if let timeStampPost = value["time stamp"] as? Float {
                        postRanker = rankers(timeStamp: timeStampPost, postUIID: postUIID, postBy: postBy)
                        if self.rankingArray.count == 0 {
                            self.rankingArray.append(postRanker)
                            self.listStory()
                        }
                        else {
                        for y in (0...(self.rankingArray.count-1)){
                            if timeStampPost >= self.rankingArray[y].timeStamp {
                                if self.rankingArray.contains(where: {$0.postUIID == postRanker.postUIID}){
                                    
                                }
                                else{
                                    self.rankingArray.insert(postRanker, at: y)
                                    self.listStory()
                                    
                                }
                            }
                            else{
                                if (y == (self.timeStampArray.count-1)){
                                    self.rankingArray.append(postRanker)
                                    self.listStory()
                                    
                              
                                }
                            }

                        }
                            }
                            }
                        }
                                }
                    }
                }
            }
        }
                    DispatchQueue.main.async {
                        
                        self.StoryCollection.reloadData()

                    }
                    
                    
    }
}

            //retrieve posts filtered
            if snap.exists() {

                for child in snap.children {
                    
                    let snapchild = child as! FIRDataSnapshot
                    let value = snapchild.value as! NSDictionary
                    if let longitude = value["longitude"] as? String {
                        let longitudeDouble = Double(longitude)
                        if longitudeDouble! >= longitudeRoundedDown {
                            if longitudeDouble! <= longitudeRoundedUp {
                        if let postBy = value["posted by"] as? String {
                        if MyVariables.filteredFriendList.contains(postBy) {
                            if let postUIID = value["postUIID"] as? String {
                                if let timeStampPost = value["time stamp"] as? Float {
                                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeStampPost/1000))
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "yyyy-MM-dd"
                                    let stringDate: String = formatter.string(from: date as Date)
                                    
                                    if let postBy = value["posted by"] as? String {
                                        
                                        postMedia = media(mediaType: "", postUIID: postUIID, content: "", timeStamp: timeStampPost, postedBy: postBy, date: stringDate)
                                if self.mediaSimplified.contains(where: { $0.postUIID == postUIID }){
                                }
                                else {
                                    if let mediaType = value["media type"] as? String {
                                        self.downloadMovie(postUIID: postUIID, completionHandler: { (urlDownloaded) in
                                            postMedia = media(mediaType: "movie", postUIID: postUIID, content: urlDownloaded, timeStamp: timeStampPost, postedBy: postBy, date: stringDate)
                                            if self.mediaSimplified.count == 0 {
                                                self.mediaSimplified.append(postMedia)
                                                if self.rankingArray.count == 1 {
                                                    DispatchQueue.main.async{
                                                        self.listStory()
                                                        self.StoryCollection.reloadData()
                                                    }
                                                }
                                            }
                                            else{
                                            for y in (0...(self.mediaSimplified.count-1)){
                                                if timeStampPost >= self.mediaSimplified[y].timeStamp {
                                                    if self.mediaSimplified.contains(where: {$0.postUIID == postMedia.postUIID}){
                                                        
                                                    }
                                                    else{
                                                        self.mediaSimplified.insert(postMedia, at: y)
                                                        if self.rankingArray.isEmpty == false {
                                                        if self.rankingArray[0].postUIID == postMedia.postUIID {
                                                            DispatchQueue.main.async{
                                                                self.listStory()
                                                                self.StoryCollection.reloadData()

                                                            }
                                                        }
                                                        }
                                                    }
                                                }
                                                else{
                                                    if (y == (self.mediaSimplified.count-1)){
                                                        self.mediaSimplified.append(postMedia)
                                                        if self.rankingArray.isEmpty == false {
                                                        if self.rankingArray[0].postUIID == postMedia.postUIID {
                                                            DispatchQueue.main.async{
                                                                self.listStory()
                                                                self.StoryCollection.reloadData()

                                                            }
                                                        }
                                                        }

                                                    }
                                                }
                                            }
                                            }
                                            if postMedia.postUIID == self.loadingPostUIID{
                                                self.i = self.i - 1
                                                self.displayPosts()
                                            }
                                        })
                                    }
                                    else{
                                        self.downloadPicture(postUIID: postUIID, completionHandler: { (imageDownloaded) in
                                            postMedia = media(mediaType: "image", postUIID: postUIID, content: imageDownloaded, timeStamp: timeStampPost, postedBy: postBy, date: stringDate)
                                            if self.mediaSimplified.count == 0 {
                                                self.mediaSimplified.append(postMedia)
                                                if self.rankingArray.count == 1 {
                                                    DispatchQueue.main.async{
                                                        self.listStory()
                                                        self.StoryCollection.reloadData()

                                                    }
                                                }
                                            }
                                            else{
                                            for y in (0...(self.mediaSimplified.count-1)){
                                                if self.rankingArray != nil {
                                                if timeStampPost >= self.mediaSimplified[y].timeStamp {
                                                    if self.mediaSimplified.contains(where: {$0.postUIID == postMedia.postUIID}){
                                                        
                                                    }
                                                    else{
                                                    self.mediaSimplified.insert(postMedia, at: y)
                                                        if self.rankingArray.isEmpty == false {
                                                        if self.rankingArray[0].postUIID == postMedia.postUIID {
                                                            DispatchQueue.main.async{
                                                                self.listStory()
                                                                self.StoryCollection.reloadData()
                                                            }
                                                        }
                                                        }
                                                    }
                                                }
                                                else{
                                                    if (y == (self.mediaSimplified.count-1)){
                                                        self.mediaSimplified.append(postMedia)
                                                        if self.rankingArray.isEmpty == false {
                                                        if self.rankingArray[0].postUIID == postMedia.postUIID {
                                                            DispatchQueue.main.async{
                                                                self.listStory()
                                                                self.StoryCollection.reloadData()
                                                                
                                                            }
                                                        }
                                                        }
                                                    }
                                                }
                                            }
                                                }
                                            }
                                            if postMedia.postUIID == self.loadingPostUIID{
                                                self.i = self.i - 1
                                                self.displayPosts()
                                            }
                                        })
                                    }
                                 
                                    
                                            }
                                        
                                }
                                }
                            }
                        }
                    }
                    }
                    }
                    }
                }
                DispatchQueue.main.async {
                    
                    self.StoryCollection.reloadData()
                    
                }
            }
        }
    }
    
    var loadingPostUIID = String()

    
    func displayPosts(){
        print(self.i)

        ActivityIndicatorCustom2.stopAnimating()
        if self.l > 0 {
            self.postView.image = nil
            self.postView.backgroundColor = UIColor.black
            self.stopVideo()
            self.l = 0
            if self.i < self.mediaSimplified.count {
                
            }
            else {
                self.l = 0
            }
        }
        
        if self.mediaSimplified.isEmpty == false{
            self.StoryCollection.isHidden = true
            self.nextPictureButton.isEnabled = true
            self.previousPictureButton.isEnabled = true
            self.addSwipe()
            if self.i < self.storySelectedPostUIID.count {

                
            //self.loadingPostUIID = self.storySelectedPostUIID[self.i]
                self.postView.backgroundColor = UIColor.darkGray
            if self.mediaSimplified.contains(where: {$0.postUIID == self.storySelectedPostUIID[self.i]}){
                let index = self.mediaSimplified.index{$0.postUIID == self.storySelectedPostUIID[self.i]}
                print(index!)


                //if self.storySelected == "Story" {
                    self.ActivityIndicatorCustom2.stopAnimating()
                let media = self.mediaSimplified[index!].content
                if let urlMovie = media as? URL {
                    

                    self.playVideo(urlVideo: urlMovie)
                    self.i = self.i + 1
                    self.l = self.l + 1
                    self.PostInfoLabel.text = self.mediaSimplified[index!].postedBy
                    self.postDate.text = self.mediaSimplified[index!].date
  
                }
                else {
                    
                    if let image = self.mediaSimplified[index!].content as? UIImage {

                    self.PostInfoLabel.text = self.mediaSimplified[index!].postedBy
                    self.postDate.text = self.mediaSimplified[index!].date
                    self.postView.image = image
                    self.i = self.i + 1
                        
                    }

                }
            }
            else{
                ActivityIndicatorCustom2.startAnimating()
                self.loadingPostUIID = self.storySelectedPostUIID[self.i]
                self.i = self.i + 1
                self.postView.backgroundColor = UIColor.darkGray
                self.postView.image = nil
            }
            }
            else{
                self.closeDisplayPost()
            }
    }

    }
    
    func closeDisplayPost(){
        self.ActivityIndicatorCustom2.stopAnimating()
        self.postView.backgroundColor = UIColor.clear
        self.postView.image = nil
        self.StoryCollection.isHidden = false
        self.PostInfoLabel.text = ""
        self.postDate.text = ""
        self.nextPictureButton.isEnabled = false
        self.previousPictureButton.isEnabled = false
        self.i = 0
        self.loadingPostUIID = ""
        self.removeSwipe()
        self.stopVideo()
    }
    

    
    
    var player = AVPlayer()
    var playerLayer = AVPlayerLayer()

    
    
    
    func playVideo(urlVideo: URL) {
        
        self.nextPictureButton.isEnabled = true
        self.previousPictureButton.isEnabled = true

        self.player = AVPlayer(url: urlVideo)
        self.playerLayer = AVPlayerLayer(player: player)
        self.playerLayer.frame = self.view.bounds
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        ActivityIndicatorCustom2.stopAnimating()
        self.postView.backgroundColor = UIColor.clear
        self.view.layer.addSublayer(playerLayer)

        self.view.bringSubview(toFront: self.PostInfoLabel)
        self.view.bringSubview(toFront: self.postDate)
        self.view.bringSubview(toFront: self.nextPictureButton)
        self.view.bringSubview(toFront: self.previousPictureButton)
       // self.view.layer.add
        player.play()
        }
    
    func stopVideo() {
        player.pause()
        self.playerLayer.removeFromSuperlayer()
    }
    

    

    @IBAction func nextPost(_ sender: Any) {
               //self.displayPosts()

    }
    
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            welcomeMapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.location = locations.last! as CLLocation
        let state = UIApplication.shared.applicationState
        if state == .background {
            
            if self.notificationCount == 0 {
              self.locationFoundBackground()
            }
        }
        else{
            self.refreshLocation()
            self.locationFound()
        }
    }
    
    func locationFoundBackground() {

        if location != nil {
            
            let latitude = location.coordinate.latitude
            let latitudeRounded = Double(10000*latitude)
            let longitude = location.coordinate.longitude
            let longitudeRounded = Double(10000*longitude)

            if self.previousLatitude == 0 {
                self.previousLatitude = latitudeRounded
                self.previousLongitude = longitudeRounded
                self.retrievePostUIIDBackground()
            }
                
            else {
                
                let latitudeRange = (latitudeRounded-0.5)..<(latitudeRounded+0.5)
                let longitudeRange = (longitudeRounded-0.5)..<(longitudeRounded+0.5)
                if latitudeRange.contains(self.previousLatitude){
                    if longitudeRange.contains(self.previousLongitude){
                        
                    }
                    else{
                        self.previousLatitude = latitudeRounded
                        self.previousLongitude = longitudeRounded
                        self.retrievePostUIIDBackground()
                    }
                }
                else{
                    self.previousLatitude = latitudeRounded
                    self.previousLongitude = longitudeRounded
                    self.retrievePostUIIDBackground()
                }
            }
        }
    }
    
    func locationFound() {
        if location != nil {
            
            let latitude = location.coordinate.latitude
            let latitudeRounded = Double(10000*latitude)
            let longitude = location.coordinate.longitude
            let longitudeRounded = Double(10000*longitude)
            //self.positionLatitude = String(latitudeRounded)
            //self.positionLongitude = String(longitudeRounded)
            
            if self.previousLatitude == 0 {
                self.previousLatitude = latitudeRounded
                self.previousLongitude = longitudeRounded
                self.retrievePostUIID()
            }
                
            else {

                    let latitudeRange = (latitudeRounded-0.5)..<(latitudeRounded+0.5)
                let longitudeRange = (longitudeRounded-0.5)..<(longitudeRounded+0.5)
                if latitudeRange.contains(self.previousLatitude){
                    if longitudeRange.contains(self.previousLongitude){
                        
                    }
                    else{
                        self.previousLatitude = latitudeRounded
                        self.previousLongitude = longitudeRounded
                        self.resetPostsNewLocation()
                       
                    }
                }
                else{
                    self.previousLatitude = latitudeRounded
                    self.previousLongitude = longitudeRounded
                    self.resetPostsNewLocation()
              
                }
            }
        }
    }
    
    
    func findFriends() {
        
       
        let refFriends = FIRDatabase.database().reference().child("friends")
        refFriends.queryOrdered(byChild: "friend1").queryEqual(toValue: MyVariables.currentUserName).observe(FIRDataEventType.value) { (snap: FIRDataSnapshot) in

            if snap.exists() {
               let numberFriends = Int(snap.childrenCount)

                
                for child in snap.children {
                    let snapchild = child as! FIRDataSnapshot
                    let value = snapchild.value as! NSDictionary
                    if let friendUsername = value["friend2"] as? String{

                        if MyVariables.friendList.contains(friendUsername){
                            if MyVariables.friendList.count == numberFriends {
                              //  self.retrievePostUIID()
                            }
                        }
                        else {
                        MyVariables.friendList.append(friendUsername)
                        MyVariables.filteredFriendList.append(friendUsername)
                            self.StoryCollection.reloadData()
                            self.downloadProfilePic(username: friendUsername, completionHandler: { (imageDownloaded) in
                                DispatchQueue.main.async{
                                    self.StoryCollection.reloadData()
                                }
                            })
                            if MyVariables.friendList.count == numberFriends {
                                //self.retrievePostUIID()
                            }
                        }
                    }
                }
            }
            DispatchQueue.main.async{
                refFriends.queryOrdered(byChild: "friend2").queryEqual(toValue: MyVariables.currentUserName).observeSingleEvent(of: FIRDataEventType.value) { (snap: FIRDataSnapshot) in
                    if snap.exists() {
                        let numberFriends = Int(snap.childrenCount)
                        for child in snap.children {
                            let snapchild = child as! FIRDataSnapshot
                            let value = snapchild.value as! NSDictionary
                            if let friendUsername = value["friend1"] as? String{
                                if MyVariables.friendList.contains(friendUsername){
                                    if MyVariables.friendList.count == numberFriends {
                                        //self.retrievePostUIID()
                                    }

                                }
                                else {
                                    MyVariables.friendList.append(friendUsername)
                                    MyVariables.filteredFriendList.append(friendUsername)
                                    self.downloadProfilePic(username: friendUsername, completionHandler: { (imageDownloaded) in
                                        DispatchQueue.main.async{
                                          self.StoryCollection.reloadData()
                                        }
                                        
                                    })
                                    self.StoryCollection.reloadData()
                                    if MyVariables.friendList.count == numberFriends {
                                       // self.retrievePostUIID()
                                    }
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async{
                        self.retrievePins()

                    }
                }
            }
        }
    }
    
    
    var listPoster : [String] = ["Story"]
    
    func downloadProfilePic (username : String, completionHandler: @escaping (_ image: UIImage) -> Void){

        let refProfilePic = FIRDatabase.database().reference().child("profilePicture")
        refProfilePic.child(username).observeSingleEvent(of: .value) { (snap : FIRDataSnapshot) in
            
            if !snap.exists() {

                return
            }
            let value = snap.value as! NSDictionary
            if let postUIID = value["postUIID"] as? String{
                let pathReference = self.storage.reference(withPath: "images/\(postUIID).jpeg")
                
                pathReference.downloadURL { url, error in
                    if let error = error {
                        print(error)
                    } else {
                        let sessionTask = URLSession.shared
                        let task = sessionTask.dataTask(with: url!, completionHandler: { (data, response, error) in
                            if data != nil{
                                if self.profilePicUsername.contains(username){
                                    //self.StoryCollection.reloadData()
                                }
                                else{
                                    
                                    let image = UIImage(data: data!)
                                    completionHandler(image!)
                                        self.profilePicUsername.append(username)
                                        self.profilePicArray.append(image!)
                                    DispatchQueue.main.async{
                                        self.StoryCollection.reloadData()
                                    }
                                }


                            }
                        })
                        task.resume()
                      
                        
                    }
                }
                
                
            }
                
            }
    }

    func listStory (){
        for rank in self.rankingArray {
            if self.listPoster.contains(rank.postBy){
                
            }
            else{
                self.listPoster.append(rank.postBy)
                DispatchQueue.main.async{
                    self.StoryCollection.reloadData()
                }
            }
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        let numberOfStories = listPoster.count
        return numberOfStories
    }
    
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! ProfileCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
         if indexPath.item < self.listPoster.count {
        cell.nameLabel.text = self.listPoster[indexPath.item]
        //var initial : String
       
            let str = self.listPoster[indexPath.item]
            let indexCh = str.index(str.startIndex, offsetBy: 0)
            let endindexCh = str.index(str.startIndex, offsetBy: 0)
            let initial = str[Range(indexCh..<endindexCh)]
        cell.initialLabel.text = initial
        }
        
        //cell.initialLabel.text = self.listPoster[indexPath.item]
        cell.backgroundColor = UIColor.white.withAlphaComponent(0)

        
        if self.profilePicUsername.contains(self.listPoster[indexPath.item]){

            let indexOfUsername = self.profilePicUsername.index(of: self.listPoster[indexPath.item])
            if self.profilePicArray.count > indexOfUsername! {
            cell.profilePic.image = self.profilePicArray[indexOfUsername!] 
            cell.initialLabel.isHidden = true
            }
        }
        else{
            if self.listPoster[indexPath.item] == "Story" {
                let image = UIImage(named: "logo 15")
                
                cell.profilePic.image = image
            }
            else{
                 cell.profilePic.image = nil
            }
           
        }
        
        

        cell.profilePic.backgroundColor = UIColor(red: 136/255, green: 189/255, blue: 186/255, alpha: 0.3)
        cell.profilePic.layer.borderWidth = 1
        cell.profilePic.layer.borderColor = UIColor(red: 245/255, green: 78/255, blue: 188/255, alpha: 1).cgColor
        cell.profilePic.layer.cornerRadius = 34.5
        cell.profilePic.clipsToBounds = true
        
        return cell
    }
    
    

    
    class profilePic {
        var username : String
        var image : UIImage
        init (username: String, image : UIImage){
            self.username = username
            self.image = image
        }
    }

    
    var profilePicArray = [UIImage]()
    var profilePicUsername = [String]()
    
    var storySelected : String = "Story"
    var storySelectedPostUIID = [String]()

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
                self.storySelectedPostUIID = []
        self.storySelected = listPoster[indexPath.item]
        self.makestory(withCompletion: self.displayPosts)
    }
    
    func makestory(withCompletion completion: () -> Void ) {
        self.i = 0
        print("story for \(self.storySelected)")
        var timeArray = [Float]()
        if self.storySelected == "Story" {
            for rank in self.rankingArray {
                if self.storySelectedPostUIID.count < 1 {
                    self.storySelectedPostUIID.append(rank.postUIID)
                    timeArray.append(rank.timeStamp)
                }
                else{
                    for y in (0...(timeArray.count-1)){
                        if rank.timeStamp >= timeArray[y] {
                            if storySelectedPostUIID.contains(rank.postUIID){
                                
                            }
                            else{
                                self.storySelectedPostUIID.insert(rank.postUIID, at: y)
                                timeArray.insert(rank.timeStamp, at: y)
                            }
                        }
                        else{
                            if (y == (self.storySelectedPostUIID.count-1)){
                                timeArray.append(rank.timeStamp)
                                self.storySelectedPostUIID.append(rank.postUIID)
                            }
                        }
                    }
                }
            }
        }
        else{
        for rank in self.rankingArray {
            
            if rank.postBy == self.storySelected {
                if self.storySelectedPostUIID.count < 1 {
                self.storySelectedPostUIID.append(rank.postUIID)
                    timeArray.append(rank.timeStamp)
                }
                else{
                    for y in (0...(timeArray.count-1)){
                        if rank.timeStamp >= timeArray[y] {
                            if storySelectedPostUIID.contains(rank.postUIID){
                                
                            }
                            else{
                        self.storySelectedPostUIID.insert(rank.postUIID, at: y)
                        timeArray.insert(rank.timeStamp, at: y)
                            }
                        }
                        else{
                            if (y == (self.storySelectedPostUIID.count-1)){
                                timeArray.append(rank.timeStamp)
                                self.storySelectedPostUIID.append(rank.postUIID)
                            }
                        }
                    }
                }
            }
        }
        }
        completion()
    }
    
    func swipeCancel(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            print("SWIPEEEE")
            ActivityIndicatorCustom2.stopAnimating()
            self.closeDisplayPost()


        }
    }
    
    
    
    func addSwipe() {
        let remove = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeCancel(_:)))
        remove.direction = .down
        view.addGestureRecognizer(remove)
    }
    
    func removeSwipe(){
        let remove = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeCancel(_:)))
        view.removeGestureRecognizer(remove)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.StoryCollection.dataSource = self
        self.StoryCollection.delegate = self
        clearTmpDirectory()
        self.ActivityIndicator.hidesWhenStopped =  true
        let mainColor = UIColor(red: 245/255, green: 78/255, blue: 188/255, alpha: 1)
        self.welcomeMapView.tintColor = mainColor
        
        self.notificationCountLabel.layer.cornerRadius = 7.5
        self.notificationCountLabel.clipsToBounds = true
        self.notificationCountLabel.isHidden = true
        
        self.welcomeMapView.layer.cornerRadius = 10
        self.welcomeMapView.clipsToBounds = true
        
        
        
        self.welcomeMapView.delegate = self
        self.previousPictureButton.titleLabel?.text = ""
        self.previousPictureButton.isEnabled = false
        self.addSwipe()
        self.nextPictureButton.titleLabel?.text = ""
        self.nextPictureButton.isEnabled = false
        
        self.downloadProfilePic(username: MyVariables.currentUserName, completionHandler: { (imageDownloaded) in
                self.StoryCollection.reloadData()
            
        })
        self.findFriends()
        self.queryFriendRequest()

        FIRAuth.auth()!.addStateDidChangeListener { auth, user in
            guard let user = user else { return }
            
            self.user = User(authData: user)
            
            //self.checkLocationAuthorizationStatus()
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.delegate=self
            //self.locationManager.desiredAccuracy=kCLLocationAccuracyBest
            self.locationManager.distanceFilter = 10
            self.locationManager.startUpdatingLocation()
            self.welcomeMapView.showsUserLocation = true
            //self.refreshLocation()
            
            //let annotationView = self.welcomeMapView.view(for: self.welcomeMapView.userLocation)
            //self.welcomeMapView.superview?.bringSubview(toFront: annotationView!)
            
            
         
        }

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: nil) { [weak self] notification in
            self?.reWindView()
        }
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: nil, using: { (_) in
            DispatchQueue.main.async {
                self.player.seek(to: kCMTimeZero)
                self.player.play()
            }
        })
    }
    
    func retrievePins (){
        let pinPostRef = FIRDatabase.database().reference(withPath: "post")
        
        for friendFiltered in MyVariables.filteredFriendList {
        
        pinPostRef.queryOrdered(byChild: "posted by").queryEqual(toValue: friendFiltered).observe(.childAdded, with: { (snapshot) in

            if snapshot.exists() {
                let value = snapshot.value as! NSDictionary
                var pinValue = MyPins()
                if let postBy = value["posted by"] as? String {
                    if MyVariables.filteredFriendList.contains(postBy){
                    pinValue.postBy = postBy
                if let latitudePin = value["latitude"] as? String {
                    let latitudePinDouble = Double(latitudePin)
                    if latitudePinDouble != nil {
                        let actualLatitude = latitudePinDouble!/10000
                        self.pinArrayLatitude.append(actualLatitude)
                        pinValue.latitude = actualLatitude
                    }
                }
                if let longitudePin = value["longitude"] as? String {
                    let longitudePinDouble = Double(longitudePin)
                    if longitudePinDouble != nil {
                        let actualLongitude = longitudePinDouble!/10000
                        self.pinArrayLongitude.append(actualLongitude)
                        pinValue.longitude = actualLongitude
                    }
                }
            }
                }
                DispatchQueue.main.async{
                    if pinValue.latitude != 0.0 {
                    self.pinsArray.append(pinValue)
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = CLLocationCoordinate2D(latitude: pinValue.latitude, longitude: pinValue.longitude)
                    annotation.title = pinValue.postBy
                    //annotation.title = String(pinValue.latitude)
                    self.welcomeMapView.addAnnotation(annotation)
                    }
                    self.StoryCollection.reloadData()
                }
                
            }

        })
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {

            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
           // annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "letter pin 3 yellow color - 130")
            let offset:CGPoint = CGPoint(x: 0, y: -annotationView.image!.size.height / 2)
            annotationView.centerOffset = offset

            
           
        }

        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is MKUserLocation {
                    //self.displayPosts()
                   mapView.deselectAnnotation(view.annotation, animated: false)
        }
    }
    
    func viewDidAppear(){
        self.reWindView()
        self.notificationCount = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func UnWindToWelcomeViewController (sender:UIStoryboardSegue){
        self.reWindView()
    }
    func reWindViewOld (){
        self.refPost.removeAllObservers()
        self.j = 0
        self.refreshLocation()
        self.previousLatitude = 0
        self.previousLongitude = 0
        self.locationFound()
        
        self.resetPosts()
        let allAnnotations = self.welcomeMapView.annotations
        self.welcomeMapView.removeAnnotations(allAnnotations)
        self.pinsArray = []
        self.findFriends()
        
        self.retrievePostUIID()
    }
    
    func reWindView (){
        self.resetPostsSameLocation()
    }
    
    func resetPosts() {
        self.mediaSimplified = []
        self.rankingArray = []
        self.listPoster = ["Story"]
    }
    
    func resetPostsNewLocation() {
        self.refPost.removeAllObservers()
        self.j = 0
        self.refreshLocation()
        self.mediaSimplified = []
        self.rankingArray = []
        self.listPoster = ["Story"]
        self.StoryCollection.reloadData()
        self.findFriends()
        self.retrievePostUIID()
    }
    
    func resetPostsSameLocation(){
        //self.refPost.removeAllObservers()
        self.findFriends()
        self.retrievePostUIID()
    }
    
    func displayPins() {
        if self.pinsArray.isEmpty == false{
            for pin in pinsArray {
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2D(latitude: pin.latitude, longitude: pin.longitude)
                annotation.title = pin.postBy
                self.welcomeMapView.addAnnotation(annotation)
            }
        }
    }
    
    func queryFriendRequest(){
        let refFriendRequest = FIRDatabase.database().reference().child("friend requests")
        
        refFriendRequest.queryOrdered(byChild: "sent to").queryEqual(toValue: MyVariables.currentUserName).observe(FIRDataEventType.value) { (snap: FIRDataSnapshot) in
            if snap.exists() {
                for child in snap.children {
                    let snapchild = child as! FIRDataSnapshot
                    let value = snapchild.value as! NSDictionary
                    if let statusRequest = value["status"] as? String{
                        if statusRequest == "pending" {
                            if let usernameRequest = value["request from"] as? String{
                                MyVariables.notificationFriendRequestCount = MyVariables.notificationFriendRequestCount + 1
                                //self.notificationCount = self.notificationCount + 1
                                self.notificationCountLabel.text = String(MyVariables.notificationFriendRequestCount)
                                self.notificationCountLabel.isHidden = false
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.locationManager.startUpdatingLocation()
        if MyVariables.notificationFriendRequestCount < 1 {
            self.notificationCountLabel.isHidden = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        // Show the navigation bar on other view controllers
        //self.locationManager.stopUpdatingLocation()
        self.refPost.removeAllObservers()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



extension ViewController:UNUserNotificationCenterDelegate {
    
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert,.badge])
    }
    
    // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case "action1":
            print()

        case "action2":
            print()

        default:
            break
        }
        completionHandler()
    }
    
}




