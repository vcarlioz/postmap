//
//  CameraViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 12/12/2016.
//  Copyright © 2016 ACLA. All rights reserved.
//

import UIKit
import MobileCoreServices
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import MapKit

class CameraViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, CLLocationManagerDelegate {
  
    @IBOutlet weak var cameraView: UIImageView!
    @IBOutlet weak var messageField: UITextField!

    var newMedia: Bool?
    let ref = FIRDatabase.database().reference()
    let storage = FIRStorage.storage()
    let storageRef = FIRStorage.storage().reference(forURL: "gs://postmap-e0514.appspot.com/")
    let username = FIRAuth.auth()?.currentUser
    var location: CLLocation!
    var locationManager = CLLocationManager()
    var postLatitude : String = ""
    var postLongitude : String = ""
    var i = 0
    

    func postLocation(){
        print(location)
        if location != nil {
            print("location")
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            self.postLatitude = String(10000*latitude)
            self.postLongitude = String(10000*longitude)
            print(self.postLatitude)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.location = locations.last! as CLLocation
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.messageField.isHidden = true
        self.checkLocationAuthorizationStatus()
        self.locationManager.delegate=self
        self.locationManager.desiredAccuracy=kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
        }
        // Do any additional setup after loading the view.
    override func viewDidAppear(_ animated: Bool) {
        self.messageField.isHidden = true
        if self.i < 1 {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            imagePicker.mediaTypes = [kUTTypeImage as String,kUTTypeMovie as String]
            imagePicker.view.frame = self.view.bounds
            imagePicker.videoQuality = UIImagePickerControllerQualityType(rawValue: 3)!
            self.view.bringSubview(toFront: self.messageField)
            
            imagePicker.restoresFocusAfterTransition = true
            
            //imagePicker.showsCameraControls = false
            self.present(imagePicker, animated: false, completion: nil)
            
            newMedia = true
            }
        }
    }
    

    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.dismiss(animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
        self.performSegue(withIdentifier: "undwindToMenu", sender: self)
        print("click CANCEL")
        self.i = self.i+1
        //dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self.postLocation()
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        print(mediaType)

        self.dismiss(animated: true, completion: nil)
  
        if mediaType.isEqual(to: kUTTypeImage as String) {
            let image = info[UIImagePickerControllerOriginalImage]
                as! UIImage
            if (newMedia == true) {
                
                print("PHOTOSAVE")
                // Data in memory
                var data = Data()
                let imageData: Data = UIImageJPEGRepresentation(image, 0.2)!
                data = imageData
                let postUIID = NSUUID.init()
                let postUIIDString = postUIID.uuidString
                let postTimeStamp = Timestamp

                print("time stamp :\(postTimeStamp)")
                
                
                
                // Create a reference to the file you want to upload
                let imagesRef = storage.reference()
                let postRef = imagesRef.child("images/\(postUIIDString).jpeg")
                
                //create post

               
                
                // Upload the file
                let uploadTask = postRef.put(data, metadata: nil) { (metadata, error) in
                    guard let metadata = metadata
                        
                        else {
                        // Uh-oh, an error occurred!
                        return
                    }
                    
                     self.ref.child("post").childByAutoId().setValue(["postUIID": postUIIDString,"latitude":self.postLatitude,"longitude":self.postLongitude,"posted by":MyVariables.currentUserName,"time stamp":postTimeStamp])
                    
                    // Metadata contains file metadata such as size, content-type, and download URL.
                    let downloadURL = metadata.downloadURL
                    self.performSegue(withIdentifier: "undwindToMenu", sender: self)
                    self.dismiss(animated: true, completion: nil)
                }
                DispatchQueue.main.async(execute: {
                    
                })
                
                
                //UIImageWriteToSavedPhotosAlbum(image, self,#selector(CameraViewController.image(image:didFinishSavingWithError:contextInfo:)), nil)
           } /*else if mediaType.isEqual(to: kUUTypeMovie) {
                // Code to support video here
            }*/
            
        }
        if mediaType.isEqual(to: kUTTypeMovie as String) {
            

            
            print("video detected")
            let videoPath = info[UIImagePickerControllerMediaURL] as? URL
            if (newMedia == true) {
                let data = Data()
                let postUIID = NSUUID.init()
                let postUIIDString = postUIID.uuidString
                let postTimeStamp = Timestamp
                
              
                
                let imagesRef = storage.reference()
                let postRef = imagesRef.child("images/\(postUIIDString).mov")
                let metadata = FIRStorageMetadata()
                
                let uploadTask = postRef.putFile(videoPath!, metadata: nil){ metadata, error in
                    if let error = error {
                        print(error)
                    } else {
                        let downloadURL = metadata!.downloadURL()
                        print("video upload")
                        self.ref.child("post").childByAutoId().setValue(["postUIID": postUIIDString,"latitude":self.postLatitude,"longitude":self.postLongitude,"media type":"movie","posted by":MyVariables.currentUserName,"time stamp":postTimeStamp])
                        self.performSegue(withIdentifier: "undwindToMenu", sender: self)
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafeRawPointer) {
        
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                                          message: "Failed to save image",
                                          preferredStyle: UIAlertControllerStyle.alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                                             style: .cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true,
                         completion: nil)
        }
    }


    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
