//
//  custom annotation.swift
//  postmap
//
//  Created by Victor Carlioz on 16/01/2017.
//  Copyright © 2017 ACLA. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class CustomPointAnnotation: MKPointAnnotation {
    var pinCustomImageName:String!
}
