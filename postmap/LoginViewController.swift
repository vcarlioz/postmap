//
//  loginViewController.swift
//  postmap
//
//  Created by Victor Carlioz on 10/12/2016.
//  Copyright © 2016 ACLA. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class LoginViewController: UIViewController {
    
    var ref: FIRDatabaseReference!
    
    let loginToMainMenu = "LoginToMainMenu"
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passcodeTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    
    @IBAction func forgotLogInButton(_ sender: Any) {
        let alert = UIAlertController(title: "Postmap",
                                      message: "Reset your password?",
                                      preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default) { action in
                                        let emailField = alert.textFields![0].text
                                        FIRAuth.auth()?.sendPasswordReset(withEmail: emailField!){(error) in
                                            let alertReset = UIAlertController(title: "Postmap",
                                                                          message: "A reset email has been sent",
                                                                          preferredStyle: .alert)
                                            let cancelAction = UIAlertAction(title: "ok",
                                                                             style: .default)
                                            alertReset.addAction(cancelAction)
                                            self.present(alertReset, animated: true, completion: nil)

                                        }
        }
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default)
        
        alert.addTextField { textEmail in
            textEmail.placeholder = "email"
        }
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
        
    }
    @IBAction func logInDidTouch(_ sender: UIButton) {
        FIRAuth.auth()!.signIn(withEmail: emailTextField.text!,
                               password: passcodeTextField.text!){ (user, error) in
                                
                                if error != nil {
                                   
                                    if let errCode = FIRAuthErrorCode(rawValue: error!._code) {
                                        
                                        switch errCode {
                                        case .errorCodeUserNotFound:
                                            let alert = UIAlertController(title: "Postmap",
                                                                          message: "Error: user not found",
                                                                          preferredStyle: .alert)
                                            let cancelAction = UIAlertAction(title: "ok",
                                                                             style: .default)
                                            alert.addAction(cancelAction)
                                            self.present(alert, animated: true, completion: nil)
                                        case .errorCodeWrongPassword:
                                            let alert = UIAlertController(title: "Postmap",
                                                                          message: "Error: wrong password",
                                                                          preferredStyle: .alert)
                                            let cancelAction = UIAlertAction(title: "ok",
                                                                             style: .default)
                                            alert.addAction(cancelAction)
                                            self.present(alert, animated: true, completion: nil)
                                            
                                        case .errorCodeInvalidEmail:
                                            print("invalid email")
                                            let alert = UIAlertController(title: "Postmap",
                                                                          message: "Error: invalid email",
                                                                          preferredStyle: .alert)
                                            let cancelAction = UIAlertAction(title: "ok",
                                                                             style: .default)
                                            alert.addAction(cancelAction)
                                            self.present(alert, animated: true, completion: nil)
                                            
                                        case .errorCodeEmailAlreadyInUse:
                                            print("in use")
                                            let alert = UIAlertController(title: "Postmap",
                                                                          message: "Error: email already in use",
                                                                          preferredStyle: .alert)
                                            let cancelAction = UIAlertAction(title: "ok",
                                                                             style: .default)
                                            alert.addAction(cancelAction)
                                            self.present(alert, animated: true, completion: nil)
                                        default:
                                            print("Create User Error: \(error!)")
                                        }
                                    }
                                }
        }
    }
    @IBAction func SignupDidTouch(_ sender: UIButton) {
        let alert = UIAlertController(title: "Postmap",
                                      message: "Please create a new account",
                                      preferredStyle: .alert)
        
        
        
        
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default) { action in
                                        
                                        
                                        
                                        let emailField = alert.textFields![0]
                                        let passwordField = alert.textFields![1]
                                        let username = alert.textFields![2]
                                        
                                        let checkWaitingRef = FIRDatabase.database().reference()
                                        checkWaitingRef.queryOrdered(byChild: "users").queryEqual(toValue: "\(username)").observe(.value, with: { (snapUser) in
                                            
                                            if ( snapUser.value is NSNull ) {
                                                print("not found)")
                                                
                                            } else {
                                               print("username already exist")
                                            }
                                        })
                                        
                                        let databaseRef = FIRDatabase.database().reference()
                                        databaseRef.child("users").queryOrdered(byChild: "username").queryEqual(toValue: username.text!).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                                           if snapshot.exists() {
                                                print("true user exist")
                                                let alertUsername = UIAlertController(title: "Postmap",
                                                                              message: "This username is already taken",
                                                    preferredStyle: .alert)
                                                let okAction = UIAlertAction(title: "ok",
                                                                             style: .default){ action in
                                                self.present(alert, animated: true, completion: nil)
                                                }
                                                alertUsername.addAction(okAction)
                                                self.present(alertUsername, animated: true, completion: nil)

                                         
                                                
                                            }
                                                
                                            else{
                                                FIRAuth.auth()!.createUser(withEmail: emailField.text!,password: passwordField.text!)
                                                { user, error in
                                                    print(error)
                                                    if error != nil {
                                                        if let errCode = FIRAuthErrorCode(rawValue: error!._code) {
                                                            switch errCode {
                                                            case .errorCodeWeakPassword:
                                                                let alert = UIAlertController(title: "Postmap",
                                                                                              message: "Error: your password is too weak it should be at least 6 characters",
                                                                                              preferredStyle: .alert)
                                                                let cancelAction = UIAlertAction(title: "ok",
                                                                                                 style: .default)
                                                                alert.addAction(cancelAction)
                                                                self.present(alert, animated: true, completion: nil)
                                                            case .errorCodeInvalidEmail:
                                                                let alert = UIAlertController(title: "Postmap",
                                                                                              message: "Error: this email is already used",
                                                                                              preferredStyle: .alert)
                                                                let cancelAction = UIAlertAction(title: "ok",
                                                                                                 style: .default)
                                                                alert.addAction(cancelAction)
                                                                self.present(alert, animated: true, completion: nil)
                                                            default:
                                                                print("Create User Error: \(error!)")
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        
                                                        
                                                        if (username != nil){
                                                            FIRAuth.auth()!.signIn(withEmail: self.emailTextField.text!,password: self.passcodeTextField.text!)
                                                            self.ref.child("users").child((user?.uid)!).setValue(["username": username.text!])
                                                            let changeRequest = FIRAuth.auth()?.currentUser?.profileChangeRequest()
                                                            changeRequest?.displayName = username.text!
                                                            changeRequest?.commitChanges() { (error) in
                                                                // ...
                                                            }
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                    }
                                                    }
                                                }
                                                print("false user doesn't exist")
                                                

                                            //}
                                            
                                            
                                            
                                            
                                        })
                                       
                                        
                                        
                                        //if passwordField.text == passwordFieldVerification.text {
                                        

                                        /*else{
                                         let alertPassword = UIAlertController(title: "passwords don't match", message: "", preferredStyle: .alert)
                                         let okAction = UIAlertAction(title: "ok",style: .default)
                                         self.present(alertPassword, animated: true, completion: nil)
                                         alertPassword.addAction(okAction)
                                         }*/
                                        
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default)
        
        alert.addTextField { textEmail in
            textEmail.placeholder = "email"
        }
        
        alert.addTextField { textPassword in
            textPassword.isSecureTextEntry = true
            textPassword.placeholder = "password"
        }
        
        alert.addTextField { textusername in
            textusername.placeholder = "choose username"
        }
        
        /*func passwordValidation(_ sender: Any){
         let textPasswordVerif = sender as! UITextField
         let textPassword = alert.textFields![1]
         var resp : UIResponder! = textPasswordVerif
         while !(resp is UIAlertController){resp = resp.next}
         let alert = resp as! UIAlertController
         alert.actions[1].isEnabled = (textPasswordVerif == textPassword)
         }*/
        
        
        /*alert.addTextField { textPasswordVerif in
         textPasswordVerif.isSecureTextEntry = true
         textPasswordVerif.placeholder = "Verify your password"
         //textPasswordVerif.addTarget(self, action: Selector(("passwordValidation:")), for: .editingChanged)
         }*/
        
        
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        //saveAction.isEnabled = false
        
        present(alert, animated: true, completion: nil)
    }
    
    /*override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }*/
    
    func clearTmpDirectory() {
        do {
            let tmpDirectory = try FileManager.default.contentsOfDirectory(atPath: NSTemporaryDirectory())
            try tmpDirectory.forEach { file in
                let path = String.init(format: "%@%@", NSTemporaryDirectory(), file)
                try FileManager.default.removeItem(atPath: path)
            }
        } catch {
            print(error)
        }
    }
    
    /*override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }*/
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
        
    
    
    override func viewDidLoad() {
        //try! FIRAuth.auth()!.signOut()
        self.clearTmpDirectory()


        MyVariables.friendList = []
        print("my friend list\(MyVariables.friendList)")
        super.viewDidLoad()
        self.ref = FIRDatabase.database().reference()
        
        self.loginButton.backgroundColor = UIColor(white: 1, alpha: 0.5)
        self.signupButton.backgroundColor = UIColor(white: 1, alpha: 0.5)
        self.emailTextField.backgroundColor = UIColor(white: 1, alpha: 0.5)
        self.passcodeTextField.backgroundColor = UIColor(white: 1, alpha: 0.5)
        
        emailTextField.placeholder = "email"
        passcodeTextField.placeholder = "password"
        passcodeTextField.isSecureTextEntry = true
        
        FIRAuth.auth()!.addStateDidChangeListener() { auth, user in
            // 2
            if user != nil {
                
                // 3
                print("login \(user)")
                let userID = FIRAuth.auth()?.currentUser?.uid
                self.ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
                    // Get user value
                    let value = snapshot.value as? NSDictionary
                    let username = value?["username"] as? String ?? ""
                    MyVariables.currentUserName = username

                        MyVariables.friendList = [username]
                        MyVariables.filteredFriendList = [username]

                    
                    print(MyVariables.currentUserName)

                        self.performSegue(withIdentifier: self.loginToMainMenu, sender: nil)
                    
                    // ...
                })
                
            }
        }
    }
    

    
    /*func findFriends() {


        let refFriends = FIRDatabase.database().reference().child("friends")
        refFriends.queryOrdered(byChild: "friend1").queryEqual(toValue: MyVariables.currentUserName).observeSingleEvent(of: FIRDataEventType.value) { (snap: FIRDataSnapshot) in
            
            if snap.exists() {
                
                
                for child in snap.children {
                    let snapchild = child as! FIRDataSnapshot
                    let value = snapchild.value as! NSDictionary
                    if let friendUsername = value["friend2"] as? String{
                        
                        if MyVariables.friendList.contains(friendUsername){
                            
                        }
                        else {
                            MyVariables.friendList.append(friendUsername)
                            MyVariables.filteredFriendList.append(friendUsername)
                            print(MyVariables.friendList)
                        }
                    }
                }
            }
            DispatchQueue.main.async{
                refFriends.queryOrdered(byChild: "friend2").queryEqual(toValue: MyVariables.currentUserName).observeSingleEvent(of: FIRDataEventType.value) { (snap: FIRDataSnapshot) in
                    if snap.exists() {
                        for child in snap.children {
                            let snapchild = child as! FIRDataSnapshot
                            let value = snapchild.value as! NSDictionary
                            if let friendUsername = value["friend1"] as? String{
                                if MyVariables.friendList.contains(friendUsername){
                                }
                                else {
                                    MyVariables.friendList.append(friendUsername)
                                    MyVariables.filteredFriendList.append(friendUsername)
                                }
                            }
                        }
                    }
                }
            }
        }
    }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
